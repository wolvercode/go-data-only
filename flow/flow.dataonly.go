package flow

import (
	"fmt"

	"bitbucket.org/billing/go-data-only/handler"
)

// DataOnly :
func DataOnly() error {
	fmt.Printf("[ Getting Jobs Data Only From Database ] : ")
	handlerProcess := handler.Process{}
	listProcess, err := handlerProcess.GetProcess()
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	if listProcess == nil {
		fmt.Println("No job found in database")
		return nil
	}

	fmt.Println(len(listProcess), "Jobs")

	for _, v := range listProcess {
		fmt.Println("[ Processing ] : { Process ID :", v.ProcessID, "}")
		handlerProcess.UpdateStatus(v.ProcessID, "4")

		handler := handler.DataOnly{ProcessProp: &v}

		closeStruct := handler.ProcessDataOnly()

		handlerProcess.CloseProcess(&closeStruct)
	}

	return nil
}
