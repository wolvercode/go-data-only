package flow

import (
	"fmt"
	"os"
	"time"

	"bitbucket.org/billing/go-data-only/handler"
	"bitbucket.org/billing/go-data-only/model"
)

// StructReportDaily : Handler Report Daily
type StructReportDaily struct {
	ProcessProp  *model.ProcessReport
	LogMatch     *[]model.StructLogMatch
	LogTapinOnly *[]model.StructLogTapinOnly
}

// ReportDaily :
func ReportDaily() error {
	fmt.Printf("[ Getting Jobs Report From Database ] : ")
	handlerProcess := handler.Process{}
	listProcess, err := handlerProcess.GetProcessReportDaily()
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	if listProcess == nil {
		fmt.Println("No job found in database")
		return nil
	}

	fmt.Println(len(listProcess), "Jobs")

	for _, v := range listProcess {
		fmt.Printf("[ Processing ] : { Process ID : %s, File : %s }\n", v.ProcessID, v.ReportPath+v.ReportFilename)
		handlerProcess.UpdateStatus(v.ProcessID, "4")

		fmt.Printf("  > [ Getting Log Match ] : ")
		logMatch, err := handlerProcess.GetLogMatch(v.BatchID)
		if err != nil {
			fmt.Println("  > Failed, with error :", err.Error())
			closeStruct := model.StructCloseReport{ProcessConf: &v, ProcessStatusID: 5, ErrorMessage: err.Error()}
			handlerProcess.CloseProcessReportDaily(&closeStruct)
			continue
		}
		fmt.Println("Success")

		fmt.Printf("  > [ Getting Log TAPIN Only ] : ")
		logTapinOnly, err := handlerProcess.GetLogTapinOnly(v.BatchID)
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			closeStruct := model.StructCloseReport{ProcessConf: &v, ProcessStatusID: 5, ErrorMessage: err.Error()}
			handlerProcess.CloseProcessReportDaily(&closeStruct)
			continue
		}
		fmt.Println("Success")

		handler := StructReportDaily{ProcessProp: &v, LogMatch: &logMatch, LogTapinOnly: &logTapinOnly}

		closeStruct := handler.ProcessReport()

		handlerProcess.CloseProcessReportDaily(&closeStruct)
	}

	return nil
}

// ProcessReport :
func (p *StructReportDaily) ProcessReport() model.StructCloseReport {
	fmt.Printf("  > [ Processing Report ] : ")

	closeStruct := model.StructCloseReport{ProcessConf: p.ProcessProp, ProcessStatusID: 4, ErrorMessage: ""}

	var err error

	timeParse, err := time.Parse("20060102150405", p.ProcessProp.StartDate)
	if err != nil {
		fmt.Println("Failed, with error :", err.Error())
		closeStruct.ProcessStatusID, closeStruct.ErrorMessage = 5, err.Error()
		return closeStruct
	}

	reportMaker := handler.ReportMaker{ProcessProp: p.ProcessProp, TimeParse: &timeParse}

	err = reportMaker.NewReport()
	if err != nil {
		fmt.Println("Failed, with error :", err.Error())
		closeStruct.ProcessStatusID, closeStruct.ErrorMessage = 5, err.Error()
		return closeStruct
	}

	reportMaker.MakeFirstSheetDaily(reportMaker.Excel)

	// Match intitialization, making directory & file Match bufio writer
	if _, err := os.Stat(p.ProcessProp.ReportPath); os.IsNotExist(err) {
		err = os.MkdirAll(p.ProcessProp.ReportPath, 0777)
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			closeStruct.ProcessStatusID, closeStruct.ErrorMessage = 5, err.Error()
			return closeStruct
		}
	}

	err = reportMaker.Excel.SaveAs(p.ProcessProp.ReportPath + p.ProcessProp.ReportFilename)
	if err != nil {
		closeStruct.ProcessStatusID, closeStruct.ErrorMessage = 5, err.Error()
		return closeStruct
	}

	closeStruct.ProcessStatusID = 7

	fmt.Println("Success")

	return closeStruct
}
