package flow

import (
	"fmt"
	"os"
	"time"

	"bitbucket.org/billing/go-data-only/handler"
	"bitbucket.org/billing/go-data-only/model"
)

// StructReportMonthly : Handler Report Monthly
type StructReportMonthly struct {
	ProcessProp             *model.ProcessReportMonthly
	LogReconMonthly         *model.StructLogReconMonthly
	LogCheckDup             *map[string]model.StructLogCekdup
	LogOCSOnly              *model.StructLogOCSOnly
	LogRecon                *map[string]model.StructLogRecon
	LogMatchToleransiGroup  *map[string]model.StructLogMatchToleransiGroup
	LogReportMonthlyTAPIN   *[]model.StructLogReportMonthly
	LogReportMonthlyOCS     *[]model.StructLogReportMonthly
	LogCekdupPartnerIDTAPIN *[]model.StructLogCekdupPartnerID
	LogCekdupPartnerIDOCS   *[]model.StructLogCekdupPartnerID
	LogCekdupTopTenTAPIN    *[]model.StructLogCekdupTopTen
	LogCekdupTopTenOCS      *[]model.StructLogCekdupTopTen
}

// ReportMonthly :
func ReportMonthly() error {
	fmt.Printf("[ Getting Jobs Report Monthly From Database ] : ")
	handlerProcess := handler.Process{}
	listProcess, err := handlerProcess.GetProcessReportMonthly()
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	if listProcess == nil {
		fmt.Println("No job found in database")
		return nil
	}

	fmt.Println(len(listProcess), "Jobs")

	for _, v := range listProcess {
		fmt.Printf("[ Processing ] : { Process ID : %s, File : %s }\n", v.ProcessID, v.ReportPath+v.ReportFilename)
		handlerProcess.UpdateStatus(v.ProcessID, "4")

		fmt.Printf("  > [ Getting Log Recon Monthly ] : ")
		logReconMonthly, err := handlerProcess.GetLogReconMonthly(v.Periode, v.MoMt, v.PostPre)
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			closeStruct := model.StructCloseReportMonthly{ProcessConf: &v, ProcessStatusID: 5, ErrorMessage: err.Error()}
			handlerProcess.CloseProcessReportMonthly(&closeStruct)
			continue
		}
		fmt.Println("Success")

		fmt.Printf("  > [ Getting Log CheckDup Monthly ] : ")
		logCheckDup, err := handlerProcess.GetLogCheckDUp(v.Periode, v.MoMt, v.PostPre)
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			closeStruct := model.StructCloseReportMonthly{ProcessConf: &v, ProcessStatusID: 5, ErrorMessage: err.Error()}
			handlerProcess.CloseProcessReportMonthly(&closeStruct)
			continue
		}
		fmt.Println("Success")

		fmt.Printf("  > [ Getting Log OCS Only ] : ")
		logOCSOnly, err := handlerProcess.GetLogOCSOnly(v.Periode, v.MoMt, v.PostPre)
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			closeStruct := model.StructCloseReportMonthly{ProcessConf: &v, ProcessStatusID: 5, ErrorMessage: err.Error()}
			handlerProcess.CloseProcessReportMonthly(&closeStruct)
			continue
		}
		fmt.Println("Success")

		fmt.Printf("  > [ Getting Log Recon ] : ")
		logRecon, err := handlerProcess.GetLogRecon(v.Periode, v.MoMt, v.PostPre)
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			closeStruct := model.StructCloseReportMonthly{ProcessConf: &v, ProcessStatusID: 5, ErrorMessage: err.Error()}
			handlerProcess.CloseProcessReportMonthly(&closeStruct)
			continue
		}
		fmt.Println("Success")

		fmt.Printf("  > [ Getting Log Match Toleransi Group ] : ")
		logMatchToleransiGroup, err := handlerProcess.GetLogMatchToleransiGroup(v.Periode, v.MoMt, v.PostPre)
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			closeStruct := model.StructCloseReportMonthly{ProcessConf: &v, ProcessStatusID: 5, ErrorMessage: err.Error()}
			handlerProcess.CloseProcessReportMonthly(&closeStruct)
			continue
		}
		fmt.Println("Success")

		fmt.Printf("  > [ Getting Log Report Monthly TAPIN ] : ")
		logReportMonthlyTAPIN, err := handlerProcess.GetLogReportMonthly(v.Periode, v.MoMt, v.PostPre, "2")
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			closeStruct := model.StructCloseReportMonthly{ProcessConf: &v, ProcessStatusID: 5, ErrorMessage: err.Error()}
			handlerProcess.CloseProcessReportMonthly(&closeStruct)
			continue
		}
		fmt.Println("Success")

		fmt.Printf("  > [ Getting Log Report Monthly OCS ] : ")
		logReportMonthlyOCS, err := handlerProcess.GetLogReportMonthly(v.Periode, v.MoMt, v.PostPre, "1")
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			closeStruct := model.StructCloseReportMonthly{ProcessConf: &v, ProcessStatusID: 5, ErrorMessage: err.Error()}
			handlerProcess.CloseProcessReportMonthly(&closeStruct)
			continue
		}
		fmt.Println("Success")

		fmt.Printf("  > [ Getting Log Cekdup Monthly OCS ] : ")
		logCheckDupPartnerIDOCS, err := handlerProcess.GetLogCheckDupPartnerID(v.Periode, v.MoMt, v.PostPre, "OCS")
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			closeStruct := model.StructCloseReportMonthly{ProcessConf: &v, ProcessStatusID: 5, ErrorMessage: err.Error()}
			handlerProcess.CloseProcessReportMonthly(&closeStruct)
			continue
		}
		fmt.Println("Success")

		fmt.Printf("  > [ Getting Log Cekdup Monthly TAPIN ] : ")
		logCheckDupPartnerIDTAPIN, err := handlerProcess.GetLogCheckDupPartnerID(v.Periode, v.MoMt, v.PostPre, "TAPIN")
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			closeStruct := model.StructCloseReportMonthly{ProcessConf: &v, ProcessStatusID: 5, ErrorMessage: err.Error()}
			handlerProcess.CloseProcessReportMonthly(&closeStruct)
			continue
		}
		fmt.Println("Success")

		fmt.Printf("  > [ Getting Log Dup Top Ten Monthly OCS ] : ")
		logCheckDupTopTenOCS, err := handlerProcess.GetLogCheckDupTopTen(v.Periode, v.MoMt, v.PostPre, "OCS")
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			closeStruct := model.StructCloseReportMonthly{ProcessConf: &v, ProcessStatusID: 5, ErrorMessage: err.Error()}
			handlerProcess.CloseProcessReportMonthly(&closeStruct)
			continue
		}
		fmt.Println("Success")

		fmt.Printf("  > [ Getting Log Dup Top Ten Monthly TAPIN ] : ")
		logCheckDupTopTenTAPIN, err := handlerProcess.GetLogCheckDupTopTen(v.Periode, v.MoMt, v.PostPre, "TAPIN")
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			closeStruct := model.StructCloseReportMonthly{ProcessConf: &v, ProcessStatusID: 5, ErrorMessage: err.Error()}
			handlerProcess.CloseProcessReportMonthly(&closeStruct)
			continue
		}
		fmt.Println("Success")

		handler := StructReportMonthly{
			ProcessProp:             &v,
			LogReconMonthly:         &logReconMonthly,
			LogCheckDup:             &logCheckDup,
			LogOCSOnly:              &logOCSOnly,
			LogRecon:                &logRecon,
			LogMatchToleransiGroup:  &logMatchToleransiGroup,
			LogReportMonthlyTAPIN:   &logReportMonthlyTAPIN,
			LogReportMonthlyOCS:     &logReportMonthlyOCS,
			LogCekdupPartnerIDTAPIN: &logCheckDupPartnerIDTAPIN,
			LogCekdupPartnerIDOCS:   &logCheckDupPartnerIDOCS,
			LogCekdupTopTenTAPIN:    &logCheckDupTopTenTAPIN,
			LogCekdupTopTenOCS:      &logCheckDupTopTenOCS,
		}

		closeStruct := handler.ProcessReport()

		handlerProcess.CloseProcessReportMonthly(&closeStruct)
	}

	return nil
}

// ProcessReport :
func (p *StructReportMonthly) ProcessReport() model.StructCloseReportMonthly {
	fmt.Printf("  > [ Processing Report ] : ")

	closeStruct := model.StructCloseReportMonthly{ProcessConf: p.ProcessProp, ProcessStatusID: 4, ErrorMessage: ""}

	var err error

	timeParse, err := time.Parse("200601", p.ProcessProp.Periode)
	if err != nil {
		fmt.Println("Failed, with error :", err.Error())
		closeStruct.ProcessStatusID, closeStruct.ErrorMessage = 5, err.Error()
		return closeStruct
	}

	reportMaker := handler.ReportMaker{
		ProcessPropMonthly: p.ProcessProp, TimeParse: &timeParse, LogReconMonthly: p.LogReconMonthly,
		LogCheckDup: p.LogCheckDup, LogOCSOnly: p.LogOCSOnly, LogRecon: p.LogRecon, LogMatchToleransiGroup: p.LogMatchToleransiGroup,
		LogReportMonthlyTAPIN: p.LogReportMonthlyTAPIN, LogReportMonthlyOCS: p.LogReportMonthlyOCS,
		LogCekdupPartnerIDTAPIN: p.LogCekdupPartnerIDTAPIN, LogCekdupPartnerIDOCS: p.LogCekdupPartnerIDOCS,
		LogCekdupTopTenTAPIN: p.LogCekdupTopTenTAPIN, LogCekdupTopTenOCS: p.LogCekdupTopTenOCS,
	}

	err = reportMaker.NewReport()
	if err != nil {
		fmt.Println("Failed, with error :", err.Error())
		closeStruct.ProcessStatusID, closeStruct.ErrorMessage = 5, err.Error()
		return closeStruct
	}

	reportMaker.MakeFirstSheetMonthly(reportMaker.Excel)
	reportMaker.MakeSecondSheet(reportMaker.Excel)
	reportMaker.MakeThirdSheet(reportMaker.Excel)

	// Match intitialization, making directory & file Match bufio writer
	if _, err := os.Stat(p.ProcessProp.ReportPath); os.IsNotExist(err) {
		err = os.MkdirAll(p.ProcessProp.ReportPath, 0777)
		if err != nil {
			fmt.Println("Failed, with error :", err.Error())
			closeStruct.ProcessStatusID, closeStruct.ErrorMessage = 5, err.Error()
			return closeStruct
		}
	}

	err = reportMaker.Excel.SaveAs(p.ProcessProp.ReportPath + p.ProcessProp.ReportFilename)
	if err != nil {
		closeStruct.ProcessStatusID, closeStruct.ErrorMessage = 5, err.Error()
		return closeStruct
	}

	closeStruct.ProcessStatusID = 7

	fmt.Println("Success")

	return closeStruct
}
