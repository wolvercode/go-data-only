package model

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"bitbucket.org/billing/go-data-only/app"
)

// GetProcess :
func (p *ProcessDataOnly) GetProcess() ([]ProcessDataOnly, error) {
	var listProcess []ProcessDataOnly

	response, err := http.Get(app.Appl.DBAPIURL + "process/dataonly/getProcess")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listProcess)

	return listProcess, nil
}

// UpdateStatus :
func (p *ProcessDataOnly) UpdateStatus(processID, processStatusID string) error {
	client := &http.Client{}
	response, err := http.NewRequest(http.MethodPut, app.Appl.DBAPIURL+"process/updateStatus/"+processID+"/"+processStatusID, bytes.NewBuffer(nil))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	_, err = client.Do(response)
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	defer io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}

// CloseProcess :
func (p *ProcessDataOnly) CloseProcess(closeStruct *ClosingStruct) error {
	jsonValue, _ := json.Marshal(closeStruct)
	response, err := http.Post(app.Appl.DBAPIURL+"process/dataonly/closeProcess/", "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}
