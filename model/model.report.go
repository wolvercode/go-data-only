package model

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"bitbucket.org/billing/go-data-only/app"
)

// GetProcessReportDaily :
func (p *ProcessReport) GetProcessReportDaily() ([]ProcessReport, error) {
	var listProcess []ProcessReport

	response, err := http.Get(app.Appl.DBAPIURL + "process/report/daily/getProcess/")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listProcess)

	return listProcess, nil
}

// CloseProcessReportDaily :
func (p *ProcessDataOnly) CloseProcessReportDaily(closeStruct *StructCloseReport) error {
	jsonValue, _ := json.Marshal(closeStruct)
	response, err := http.Post(app.Appl.DBAPIURL+"process/report/daily/closeProcess/", "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}

// GetLogMatch :
func (p *ProcessReport) GetLogMatch(batchID string) ([]StructLogMatch, error) {
	var output []StructLogMatch

	response, err := http.Get(app.Appl.DBAPIURL + "process/report/daily/getLogMatch/" + batchID)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&output)

	return output, nil
}

// GetLogTapinOnly :
func (p *ProcessReport) GetLogTapinOnly(batchID string) ([]StructLogTapinOnly, error) {
	var output []StructLogTapinOnly

	response, err := http.Get(app.Appl.DBAPIURL + "process/report/daily/getLogTapinOnly/" + batchID)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&output)

	return output, nil
}

// GetProcessReportMonthly :
func (p *ProcessReport) GetProcessReportMonthly() ([]ProcessReportMonthly, error) {
	var listProcess []ProcessReportMonthly

	response, err := http.Get(app.Appl.DBAPIURL + "process/report/monthly/getProcess/")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listProcess)

	return listProcess, nil
}

// GetLogReconMonthly :
func (p *ProcessReport) GetLogReconMonthly(periode, moMt, postPre string) (StructLogReconMonthly, error) {
	var output StructLogReconMonthly

	response, err := http.Get(app.Appl.DBAPIURL + "process/report/monthly/getLogReconMonthly/" + periode + "/" + moMt + "/" + postPre)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return output, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&output)

	return output, nil
}

// GetLogCheckdup :
func (p *ProcessReport) GetLogCheckdup(periode, moMt, postPre string) (map[string]StructLogCekdup, error) {
	var output map[string]StructLogCekdup

	response, err := http.Get(app.Appl.DBAPIURL + "process/report/monthly/getLogCheckdup/" + periode + "/" + moMt + "/" + postPre)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&output)

	return output, nil
}

// GetLogOCSOnly :
func (p *ProcessReport) GetLogOCSOnly(periode, moMt, postPre string) (StructLogOCSOnly, error) {
	var output StructLogOCSOnly

	response, err := http.Get(app.Appl.DBAPIURL + "process/report/monthly/getLogOCSOnly/" + periode + "/" + moMt + "/" + postPre)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return output, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&output)

	return output, nil
}

// GetLogRecon :
func (p *ProcessReport) GetLogRecon(periode, moMt, postPre string) (map[string]StructLogRecon, error) {
	var output map[string]StructLogRecon

	response, err := http.Get(app.Appl.DBAPIURL + "process/report/monthly/getLogRecon/" + periode + "/" + moMt + "/" + postPre)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return output, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&output)

	return output, nil
}

// GetLogMatchToleransiGroup :
func (p *ProcessReport) GetLogMatchToleransiGroup(periode, moMt, postPre string) (map[string]StructLogMatchToleransiGroup, error) {
	var output map[string]StructLogMatchToleransiGroup

	response, err := http.Get(app.Appl.DBAPIURL + "process/report/monthly/getLogMatchToleransiGroup/" + periode + "/" + moMt + "/" + postPre)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return output, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&output)

	return output, nil
}

// GetLogReportMonthly :
func (p *ProcessReport) GetLogReportMonthly(periode, moMt, postPre, dataTypeID string) ([]StructLogReportMonthly, error) {
	var output []StructLogReportMonthly

	response, err := http.Get(app.Appl.DBAPIURL + "process/report/monthly/getLogReportMonthly/" + periode + "/" + moMt + "/" + postPre + "/" + dataTypeID)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return output, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&output)

	return output, nil
}

// GetLogCheckDupPartnerID :
func (p *ProcessReport) GetLogCheckDupPartnerID(periode, moMt, postPre, dataType string) ([]StructLogCekdupPartnerID, error) {
	var output []StructLogCekdupPartnerID

	response, err := http.Get(app.Appl.DBAPIURL + "process/report/monthly/getLogCheckDupPartnerID/" + periode + "/" + moMt + "/" + postPre + "/" + dataType)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return output, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&output)

	return output, nil
}

// GetLogCheckDupTopTen :
func (p *ProcessReport) GetLogCheckDupTopTen(periode, moMt, postPre, dataType string) ([]StructLogCekdupTopTen, error) {
	var output []StructLogCekdupTopTen

	response, err := http.Get(app.Appl.DBAPIURL + "process/report/monthly/getLogCheckDupTopTen/" + periode + "/" + moMt + "/" + postPre + "/" + dataType)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return output, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&output)

	return output, nil
}

// CloseProcessReportMonthly :
func (p *ProcessDataOnly) CloseProcessReportMonthly(closeStruct *StructCloseReportMonthly) error {
	jsonValue, _ := json.Marshal(closeStruct)
	response, err := http.Post(app.Appl.DBAPIURL+"process/report/monthly/closeProcess/", "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}
