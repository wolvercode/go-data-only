package model

// ProcessDataOnly :
type ProcessDataOnly struct {
	ProcessID        string `json:"process_id"`
	BatchID          string `json:"batch_id"`
	Periode          string `json:"periode"`
	DataTypeID       int    `json:"data_type_id"`
	MoMt             string `json:"mo_mt"`
	PostPre          string `json:"post_pre"`
	FileTypeDB       int    `json:"filetype_id_db"`
	PathDB           string `json:"path_db"`
	MatchDB          string `json:"match_db"`
	FileTypeDataOnly int    `json:"filetype_id_data_only"`
	PathDataOnly     string `json:"path_data_only"`
	FilenameDataOnly string `json:"filename_data_only"`
}

// ClosingStruct :
type ClosingStruct struct {
	ProcessConf        *ProcessDataOnly            `json:"process_conf"`
	ProcessStatusID    int                         `json:"process_status_id"`
	ErrorMessage       string                      `json:"error_message"`
	CountInputRecord   int                         `json:"count_input_record"`
	CountInputDuration int                         `json:"count_input_duration"`
	CountOnlyRecord    int                         `json:"count_only_record"`
	CountOnlyDuration  int                         `json:"count_only_duration"`
	CountMatchRecord   int                         `json:"count_match_record"`
	CountMatchDuration int                         `json:"count_match_duration"`
	LogPartnerID       map[string]*LogMatchContent `json:"log_partnerid"`
}

// LogMatchContent :
type LogMatchContent struct {
	DurationsInput int     `json:"durations_input"`
	RecordsInput   int     `json:"records_input"`
	RevenuesInput  float64 `json:"reveues_input"`
	DurationsMatch int     `json:"durations_match"`
	RecordsMatch   int     `json:"records_match"`
	RevenuesMatch  float64 `json:"reveues_match"`
	DurationsOnly  int     `json:"durations_only"`
	RecordsOnly    int     `json:"records_only"`
	RevenuesOnly   float64 `json:"reveues_only"`
}

// ProcessReport :
type ProcessReport struct {
	ProcessID         string `json:"process_id"`
	BatchID           string `json:"batch_id"`
	Periode           string `json:"periode"`
	Day               string `json:"day"`
	StartDate         string `json:"startdate"`
	DataTypeID        string `json:"data_type_id"`
	MoMt              string `json:"mo_mt"`
	PostPre           string `json:"post_pre"`
	FilenameSource    string `json:"filename_source"`
	ReportID          int    `json:"report_id"`
	ReportPath        string `json:"report_path"`
	ReportFilename    string `json:"report_filename"`
	SourceInput       int    `json:"source_input"`
	SplitMO           int    `json:"split_mo"`
	SplitMT           int    `json:"split_mt"`
	SplitReject       int    `json:"split_reject"`
	SplitFilename     string `json:"split_filename"`
	LookupInput       int    `json:"lookup_input"`
	LookupReject      int    `json:"lookup_reject"`
	LookupOutput      int    `json:"lookup_output"`
	LookupFilename    string `json:"lookup_filename"`
	CekdupInput       int    `json:"cekdup_input"`
	CekdupReject      int    `json:"cekdup_reject"`
	CekdupDuplicate   int    `json:"cekdup_duplicate"`
	CekdupUnduplicate int    `json:"cekdup_unduplicate"`
	CekdupFilename    string `json:"cekdup_filename"`
	VolcompInput      int    `json:"volcomp_input"`
	VolcompMatch      int    `json:"volcomp_match"`
	VolcompTapinOnly  int    `json:"volcomp_tapin_only"`
	FileTapinOnly     string `json:"file_tapin_only"`
	RejectFieldEmpty  int    `json:"field_empty"`
	RejectImsi        int    `json:"imsi"`
	RejectMOPartnerID int    `json:"mo_partner_id"`
	RejectDurasi0     int    `json:"durasi_0"`
	RejectTapCode     int    `json:"tap_code"`
	LookupRejectFile  string `json:"lookup_reject_file"`
	FilterTapCode     string `json:"filter_tapcode"`
}

// StructLogMatch :
type StructLogMatch struct {
	ProcessID   string `json:"process_id"`
	BatchID     string `json:"batch_id"`
	IDToleransi int    `json:"id_toleransi"`
	Startcall   int    `json:"startcall"`
	Durasi      int    `json:"durasi"`
	TotalMatch  int    `json:"total_match"`
}

// StructLogTapinOnly :
type StructLogTapinOnly struct {
	ProcessID     string `json:"process_id"`
	BatchID       string `json:"batch_id"`
	PartnerID     string `json:"partner_id"`
	TotalRecord   int    `json:"total_record"`
	TotalDuration int    `json:"total_duration"`
}

// StructCloseReport :
type StructCloseReport struct {
	ProcessConf     *ProcessReport `json:"process_conf"`
	ProcessStatusID int            `json:"process_status_id"`
	ErrorMessage    string         `json:"error_message"`
}

// ProcessReportMonthly :
type ProcessReportMonthly struct {
	ProcessID      string `json:"process_id"`
	BatchID        string `json:"batch_id"`
	Periode        string `json:"periode"`
	MoMt           string `json:"mo_mt"`
	PostPre        string `json:"post_pre"`
	ReportID       int    `json:"report_id"`
	ReportPath     string `json:"report_path"`
	ReportFilename string `json:"report_filename"`
	FilterTapCode  string `json:"filter_tapcode"`
}

// StructLogReconMonthly :
type StructLogReconMonthly struct {
	Periode           string `json:"periode"`
	MoMt              string `json:"mo_mt"`
	PostPre           string `json:"post_pre"`
	Input             int    `json:"input"`
	Match             int    `json:"match"`
	TapinOnly         int    `json:"tapin_only"`
	TapinOnlyDuration int    `json:"tapin_only_duration"`
	MatchDuration     int    `json:"match_duration"`
	SplitInput        int    `json:"split_input"`
	SplitMo           int    `json:"split_mo"`
	SplitMt           int    `json:"split_mt"`
	SplitReject       int    `json:"split_reject"`
	Duplicate         int    `json:"duplicate"`
	LookupReject      int    `json:"lookup_reject"`
}

// StructLogCekdup :
type StructLogCekdup struct {
	Periode             string `json:"periode"`
	DataType            string `json:"data_type"`
	MoMt                string `json:"mo_mt"`
	PostPre             string `json:"post_pre"`
	Input               int    `json:"input"`
	Duplicate           int    `json:"duplicate"`
	Unduplicate         int    `json:"unduplicate"`
	DuplicateDuration   int    `json:"duplicate_duration"`
	UnduplicateDuration int    `json:"unduplicate_duration"`
}

// StructLogOCSOnly :
type StructLogOCSOnly struct {
	Periode       string `json:"periode"`
	MoMt          string `json:"mo_mt"`
	PostPre       string `json:"post_pre"`
	InputRecord   int    `json:"input_record"`
	InputDuration int    `json:"input_duration"`
	OnlyRecord    int    `json:"only_record"`
	OnlyDuration  int    `json:"only_duration"`
	MatchRecord   int    `json:"match_record"`
	MatchDuration int    `json:"match_duration"`
	SplitInput    int    `json:"split_input"`
	SplitMo       int    `json:"split_mo"`
	SplitMt       int    `json:"split_mt"`
	SplitReject   int    `json:"split_reject"`
	Duplicate     int    `json:"duplicate"`
	LookupReject  int    `json:"lookup_reject"`
}

// StructLogRecon :
type StructLogRecon struct {
	Periode            string  `json:"periode"`
	PostPre            string  `json:"post_pre"`
	MoMt               string  `json:"mo_mt"`
	PartnerID          string  `json:"partner_id"`
	TapinInputRecord   int     `json:"tapin_input_record"`
	TapinInputDuration int     `json:"tapin_input_duration"`
	TapinOnlyRecord    int     `json:"tapin_only_record"`
	TapinOnlyDuration  int     `json:"tapin_only_duration"`
	TapinOnlyRevenue   float64 `json:"tapin_only_revenue"`
	TapinMatchRecord   int     `json:"tapin_match_record"`
	TapinMatchDuration int     `json:"tapin_match_duration"`
	TapinMatchRevenue  float64 `json:"tapin_match_revenue"`
	OcsInputRecord     int     `json:"ocs_input_record"`
	OcsInputDuration   int     `json:"ocs_input_duration"`
	OcsOnlyRecord      int     `json:"ocs_only_record"`
	OcsOnlyDuration    int     `json:"ocs_only_duration"`
	OcsMatchRecord     int     `json:"ocs_match_record"`
	OcsMatchDuration   int     `json:"ocs_match_duration"`
	OcsOnlyRevenue     float64 `json:"ocs_only_revenue"`
	OcsMatchRevenue    float64 `json:"ocs_match_revenue"`
	OcsInputRevenue    float64 `json:"ocs_input_revenue"`
}

// StructLogMatchToleransiGroup :
type StructLogMatchToleransiGroup struct {
	Periode           string `json:"periode"`
	PostPre           string `json:"post_pre"`
	MoMt              string `json:"mo_mt"`
	PartnerID         string `json:"partner_id"`
	Record0           int    `json:"record_0"`
	Record30          int    `json:"record_30"`
	Record60          int    `json:"record_60"`
	Record120         int    `json:"record_120"`
	RecordBigger120   int    `json:"record_bigger_120"`
	Duration0         int    `json:"duration_0"`
	Duration30        int    `json:"duration_30"`
	Duration60        int    `json:"duration_60"`
	Duration120       int    `json:"duration_120"`
	DurationBigger120 int    `json:"duration_bigger_120"`
}

// StructLogReportMonthly :
type StructLogReportMonthly struct {
	BatchID                  string `json:"batch_id"`
	Periode                  string `json:"periode"`
	Day                      string `json:"day"`
	DataTypeID               string `json:"data_type_id"`
	MoMt                     string `json:"mo_mt"`
	PostPre                  string `json:"post_pre"`
	FilenameCDR              string `json:"filename_cdr"`
	SplitInput               int    `json:"split_input"`
	SplitReject              int    `json:"split_reject"`
	SplitMO                  int    `json:"split_mo"`
	SplitMT                  int    `json:"split_mt"`
	SplitFilename            string `json:"split_filename"`
	LookupInput              int    `json:"lookup_input"`
	LookupReject             int    `json:"lookup_reject"`
	LookupOutput             int    `json:"lookup_output"`
	LookupFilename           string `json:"lookup_filename"`
	CekdupInput              int    `json:"cekdup_input"`
	CekdupDuplicate          int    `json:"cekdup_duplicate"`
	CekdupUnduplicate        int    `json:"cekdup_unduplicate"`
	CekdupFilename           string `json:"cekdup_filename"`
	VolcompInput             int    `json:"volcomp_input"`
	VolcompMatch             int    `json:"volcomp_match"`
	VolcompTapinOnly         int    `json:"volcomp_tapin_only"`
	VolcompTapinOnlyDuration int    `json:"volcomp_tapin_only_duration"`
	FileTapinOnly            string `json:"file_tapin_only"`
	RejectFieldEmpty         int    `json:"field_empty"`
	RejectImsi               int    `json:"imsi"`
	RejectDurasi0            int    `json:"durasi_0"`
	RejectTapCode            int    `json:"tap_code"`
	RejectMOPartnerID        int    `json:"mo_partner_id"`
	RejectImsiDurasi         int    `json:"imsi_durasi"`
	RejectTapCodeDurasi      int    `json:"tap_code_durasi"`
	RejectMOPartnerIDDurasi  int    `json:"mo_partner_id_durasi"`
	LookupRejectFilename     string `json:"lookup_reject_filename"`
}

// StructLogCekdupPartnerID :
type StructLogCekdupPartnerID struct {
	Periode             string `json:"periode"`
	DataType            string `json:"data_type"`
	MoMt                string `json:"mo_mt"`
	PostPre             string `json:"post_pre"`
	PartnerID           string `json:"partner_id"`
	Input               int    `json:"input"`
	Duplicate           int    `json:"duplicate"`
	Unduplicate         int    `json:"unduplicate"`
	DuplicateDuration   int    `json:"duplicate_duration"`
	UnduplicateDuration int    `json:"unduplicate_duration"`
}

// StructLogCekdupTopTen :
type StructLogCekdupTopTen struct {
	Periode     string `json:"periode"`
	PostPre     string `json:"post_pre"`
	MoMt        string `json:"mo_mt"`
	Imsi        string `json:"imsi"`
	BNumber     string `json:"bnumber"`
	Starttime   string `json:"starttime"`
	PartnerID   string `json:"partner_id"`
	RecordCount int    `json:"record_count"`
	SumDuration int    `json:"sum_duration"`
}

// StructCloseReportMonthly :
type StructCloseReportMonthly struct {
	ProcessConf     *ProcessReportMonthly `json:"process_conf"`
	ProcessStatusID int                   `json:"process_status_id"`
	ErrorMessage    string                `json:"error_message"`
}
