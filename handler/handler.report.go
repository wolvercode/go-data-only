package handler

import (
	"strings"
	"time"

	"bitbucket.org/billing/go-data-only/cmd"
	"bitbucket.org/billing/go-data-only/model"
	"github.com/360EntSecGroup-Skylar/excelize"
)

// ReportMaker : Handler Report Maker
type ReportMaker struct {
	ProcessProp             *model.ProcessReport
	ProcessPropMonthly      *model.ProcessReportMonthly
	LogMatch                *[]model.StructLogMatch
	LogTapinOnly            *[]model.StructLogTapinOnly
	LogReconMonthly         *model.StructLogReconMonthly
	LogCheckDup             *map[string]model.StructLogCekdup
	LogOCSOnly              *model.StructLogOCSOnly
	LogRecon                *map[string]model.StructLogRecon
	LogMatchToleransiGroup  *map[string]model.StructLogMatchToleransiGroup
	LogReportMonthlyTAPIN   *[]model.StructLogReportMonthly
	LogReportMonthlyOCS     *[]model.StructLogReportMonthly
	LogCekdupPartnerIDTAPIN *[]model.StructLogCekdupPartnerID
	LogCekdupPartnerIDOCS   *[]model.StructLogCekdupPartnerID
	LogCekdupTopTenTAPIN    *[]model.StructLogCekdupTopTen
	LogCekdupTopTenOCS      *[]model.StructLogCekdupTopTen
	Excel                   *excelize.File
	MapStyle                map[string]int
	TimeParse               *time.Time
}

var excelCustomCmd cmd.ExcelCustomCmd

// NewReport :
func (p *ReportMaker) NewReport() error {
	var err error

	p.Excel, err = excelCustomCmd.NewExcel()
	if err != nil {
		return err
	}

	p.MapStyle = cmd.ExcelCustomStyle

	p.Excel.SetActiveSheet(1)

	return nil
}

// MakeFirstSheetDaily :
func (p *ReportMaker) MakeFirstSheetDaily(f *excelize.File) {
	usedVindex := 0
	// Create a new sheet.
	index := f.NewSheet("Sheet1")
	f.SetSheetName("Sheet1", "page 1 | Preprocessing Steps")
	f.SetColWidth("page 1 | Preprocessing Steps", "A", "B", 30)
	f.SetColWidth("page 1 | Preprocessing Steps", "C", "G", 22)
	f.SetColWidth("page 1 | Preprocessing Steps", "H", "H", 32)
	f.SetColWidth("page 1 | Preprocessing Steps", "I", "M", 22)
	f.SetColWidth("page 1 | Preprocessing Steps", "N", "N", 32)
	f.SetColWidth("page 1 | Preprocessing Steps", "O", "R", 22)
	f.SetColWidth("page 1 | Preprocessing Steps", "S", "S", 32)

	mapReconTitle := map[string]map[string]interface{}{
		"A": {
			"Value":     "RECONCILLIATION REPORTS",
			"MergeTill": "C",
			"Styles":    p.MapStyle["partHeaderNoBorder"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapReconTitle)

	mapHeader2 := map[int]map[string]map[string]interface{}{
		1: {
			"A": {
				"Value":  "Recon Call Type",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     "Roaming Voice  -  " + strings.ToUpper(p.ProcessProp.MoMt) + " Only",
				"MergeTill": "C",
				"Styles":    p.MapStyle["style"],
			},
		},
		2: {
			"A": {
				"Value":  "Recon Runtime",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     p.TimeParse.Format("Monday, 02 January 2006"),
				"MergeTill": "C",
				"Styles":    p.MapStyle["style"],
			},
		},
		// 3: {
		// 	"A": {
		// 		"Value":  "Recon Range Period",
		// 		"Styles": p.MapStyle["style"],
		// 	},
		// 	"B": {
		// 		"Value":     "1 days | " + timeParse.Format("02 January 2006"),
		// 		"MergeTill": "C",
		// 		"Styles":    p.MapStyle["style"],
		// 	},
		// },
		4: {
			"A": {
				"Value":  "Recon TAPCODE",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     p.ProcessProp.FilterTapCode,
				"MergeTill": "C",
				"Styles":    p.MapStyle["style"],
			},
		},
		5: {
			"A": {
				"Value":  "PAGE",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     "Page 1 | Preprocessing Steps",
				"MergeTill": "C",
				"Styles":    p.MapStyle["style"],
			},
		},
	}

	excelCustomCmd.SetCellMultiple(f, index, &usedVindex, mapHeader2)

	usedVindex++ // skip one row

	mapTapinProcessingStepsTitle := map[string]map[string]interface{}{
		"A": {
			"Value":     "TAPIN :: File Processing Steps",
			"MergeTill": "B",
			"Styles":    p.MapStyle["partHeaderNoBorder"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingStepsTitle)

	mapTapinProcessingStepsMainHeader := map[string]map[string]interface{}{
		"A": {
			"Value":     "INPUT FILES",
			"MergeTill": "C",
			"Styles":    p.MapStyle["mainHeaderStyleDark"],
		},
		"D": {
			"Value":     "SPLIT MO/MT",
			"MergeTill": "I",
			"Styles":    p.MapStyle["mainHeaderStyleLight"],
		},
		"J": {
			"Value":     "LOOKUP",
			"MergeTill": "N",
			"Styles":    p.MapStyle["mainHeaderStyleDark"],
		},
		"O": {
			"Value":     "CHECK DUPLICATE",
			"MergeTill": "S",
			"Styles":    p.MapStyle["mainHeaderStyleLight"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingStepsMainHeader)

	mapTapinProcessingStepsSubHeader := map[string]map[string]interface{}{
		"A": {
			"Value":  "file_cdr",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "contents",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "record_count",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "split_input",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  "split_reject",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"F": {
			"Value":  "split_mt_output",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"G": {
			"Value":  "split_mo_output",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"H": {
			"Value":  "split_contents",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"I": {
			"Value":  "split_output_file",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"J": {
			"Value":  "lookup_input",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"K": {
			"Value":  "lookup_reject",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"L": {
			"Value":  "lookup_output",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"M": {
			"Value":  "lookup_contents",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"N": {
			"Value":  "lookup_output_files",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"O": {
			"Value":  "cekdup_input",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"P": {
			"Value":  "cekdup_duplicate",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"Q": {
			"Value":  "cekdup_unique",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"R": {
			"Value":  "cekdup_contents",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"S": {
			"Value":  "cekdup_output_files",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingStepsSubHeader)

	mapTapinProcessingBodyValues := map[string]map[string]interface{}{
		"A": {
			"Value":  p.ProcessProp.FilenameSource,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"B": {
			"Value":  strings.ToUpper(p.ProcessProp.MoMt),
			"Styles": p.MapStyle["bodyStyle"],
		},
		"C": {
			"Value":  p.ProcessProp.SourceInput,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"D": {
			"Value":  p.ProcessProp.SourceInput,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"E": {
			"Value":  p.ProcessProp.SplitReject,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"F": {
			"Value":  p.ProcessProp.SplitMT,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"G": {
			"Value":  p.ProcessProp.SplitMO,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"H": {
			"Value":  strings.ToUpper(p.ProcessProp.MoMt),
			"Styles": p.MapStyle["bodyStyle"],
		},
		"I": {
			"Value":  p.ProcessProp.SplitFilename,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"J": {
			"Value":  p.ProcessProp.LookupInput,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"K": {
			"Value":  p.ProcessProp.LookupReject,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"L": {
			"Value":  p.ProcessProp.LookupOutput,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"M": {
			"Value":  strings.ToUpper(p.ProcessProp.MoMt) + ", " + strings.ToUpper(p.ProcessProp.PostPre),
			"Styles": p.MapStyle["bodyStyle"],
		},
		"N": {
			"Value":  p.ProcessProp.LookupFilename,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"O": {
			"Value":  p.ProcessProp.CekdupInput,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"P": {
			"Value":  p.ProcessProp.CekdupDuplicate,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"Q": {
			"Value":  p.ProcessProp.CekdupUnduplicate,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"R": {
			"Value":  strings.ToUpper(p.ProcessProp.MoMt) + ", " + strings.ToUpper(p.ProcessProp.PostPre),
			"Styles": p.MapStyle["bodyStyle"],
		},
		"S": {
			"Value":  p.ProcessProp.CekdupFilename,
			"Styles": p.MapStyle["bodyStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingBodyValues)

	usedVindex++ // skip one row

	mapTapinProcessingStatisticsTitle := map[string]map[string]interface{}{
		"A": {
			"Value":     "TAPIN :: File Processing Statistics",
			"MergeTill": "B",
			"Styles":    p.MapStyle["partHeaderNoBorder"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingStatisticsTitle)

	mapTapinProcessingStatisticsMainHeader := map[string]map[string]interface{}{
		"A": {
			"Value":     "SPLIT MO/MT",
			"MergeTill": "B",
			"Styles":    p.MapStyle["mainHeaderStyleDark"],
		},
		"C": {
			"Value":     "LOOKUP",
			"MergeTill": "D",
			"Styles":    p.MapStyle["mainHeaderStyleLight"],
		},
		"E": {
			"Value":     "CHECK DUPLICATE",
			"MergeTill": "F",
			"Styles":    p.MapStyle["mainHeaderStyleDark"],
		},
	}

	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingStatisticsMainHeader)

	mapTapinProcessingStatisticsSubHeader := map[string]map[string]interface{}{
		"A": {
			"Value":  "Criteria",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "%",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "Criteria",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "%",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  "Criteria",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"F": {
			"Value":  "%",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}

	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingStatisticsSubHeader)

	mapTapinProcessingStatisticsBody := map[int]map[string]map[string]interface{}{
		1: {
			"A": {
				"Value":  "Reject",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  (float32(p.ProcessProp.SplitReject) / float32(p.ProcessProp.SourceInput)),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
			"C": {
				"Value":  "Reject",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  (float32(p.ProcessProp.LookupReject) / float32(p.ProcessProp.LookupInput)),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
			"E": {
				"Value":  "Duplicate",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  (float32(p.ProcessProp.CekdupDuplicate) / float32(p.ProcessProp.CekdupInput)),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
		},
		2: {
			"A": {
				"Value":  "MT",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  (float32(p.ProcessProp.SplitMT) / float32(p.ProcessProp.SourceInput)),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
			"C": {
				"Value":  "Output",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  (float32(p.ProcessProp.LookupOutput) / float32(p.ProcessProp.LookupInput)),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
			"E": {
				"Value":  "Unduplicate",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  (float32(p.ProcessProp.CekdupUnduplicate) / float32(p.ProcessProp.CekdupInput)),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
		},
		3: {
			"A": {
				"Value":  "MO",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  (float32(p.ProcessProp.SplitMO) / float32(p.ProcessProp.SourceInput)),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
		},
	}
	excelCustomCmd.SetCellMultiple(f, index, &usedVindex, mapTapinProcessingStatisticsBody)

	usedVindex++ // skip one row

	mapTapinLookupRejectTitle := map[string]map[string]interface{}{
		"A": map[string]interface{}{
			"Value":     "TAPIN :: Lookup Reject Details",
			"MergeTill": "B",
			"Styles":    p.MapStyle["partHeaderNoBorder"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinLookupRejectTitle)

	mapTapinDetailRejectLookupHeader := map[string]map[string]interface{}{
		"A": {
			"Value":  "lookup_input_file",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "reject_total",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "field_empty",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "durasi_0",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  "tapcode_notfound",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"F": {
			"Value":  "imsi_notfound",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"G": {
			"Value":  "partner_id_notallowed",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"H": {
			"Value":  "lookup_reject_output_file",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinDetailRejectLookupHeader)

	mapTapinDetailRejectLookupBody := map[string]map[string]interface{}{
		"A": {
			"Value":  p.ProcessProp.LookupFilename,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"B": {
			"Value":  p.ProcessProp.LookupReject,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"C": {
			"Value":  p.ProcessProp.RejectFieldEmpty,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"D": {
			"Value":  p.ProcessProp.RejectDurasi0,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"E": {
			"Value":  p.ProcessProp.RejectTapCode,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"F": {
			"Value":  p.ProcessProp.RejectImsi,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"G": {
			"Value":  p.ProcessProp.RejectMOPartnerID,
			"Styles": p.MapStyle["bodyStyle"],
		},
		"H": {
			"Value":  p.ProcessProp.LookupRejectFile,
			"Styles": p.MapStyle["bodyStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinDetailRejectLookupBody)

	mapTapinDetailRejectLookupPercentage := map[string]map[string]interface{}{
		"A": {
			"Value":  "Percentage",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "-",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  (float32(p.ProcessProp.RejectFieldEmpty) / float32(p.ProcessProp.LookupReject)),
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"D": {
			"Value":  (float32(p.ProcessProp.RejectDurasi0) / float32(p.ProcessProp.LookupReject)),
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"E": {
			"Value":  (float32(p.ProcessProp.RejectTapCode) / float32(p.ProcessProp.LookupReject)),
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"F": {
			"Value":  (float32(p.ProcessProp.RejectImsi) / float32(p.ProcessProp.LookupReject)),
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"G": {
			"Value":  (float32(p.ProcessProp.RejectMOPartnerID) / float32(p.ProcessProp.LookupReject)),
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"H": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinDetailRejectLookupPercentage)
}
