package handler

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/billing/go-data-only/model"
	"github.com/syndtr/goleveldb/leveldb"
)

// DataOnly : Handler DataOnly
type DataOnly struct {
	ProcessProp *model.ProcessDataOnly
}

// ProcessDataOnly :
func (p *DataOnly) ProcessDataOnly() model.ClosingStruct {
	fmt.Println("  > [ Processing Data Only ]")

	closeStruct := model.ClosingStruct{ProcessConf: p.ProcessProp, ProcessStatusID: 4, ErrorMessage: ""}

	closeStruct.LogPartnerID = make(map[string]*model.LogMatchContent)

	// Checking MatchDB exists or not
	if _, err := os.Stat(p.ProcessProp.MatchDB); os.IsNotExist(err) {
		fmt.Println("    > Failed, with error =>", err.Error())
		closeStruct.ProcessStatusID, closeStruct.ErrorMessage = 5, err.Error()
		return closeStruct
	}

	dbconn, err := leveldb.OpenFile(p.ProcessProp.PathDB, nil)
	if err != nil {
		fmt.Println("    > Failed To Open LevelDB, with error =>", err.Error())
		closeStruct.ProcessStatusID, closeStruct.ErrorMessage = 5, err.Error()
		return closeStruct
	}

	matchDB, err := leveldb.OpenFile(p.ProcessProp.MatchDB, nil)
	if err != nil {
		fmt.Println("    > Failed To Open LevelDB, with error =>", err.Error())
		closeStruct.ProcessStatusID, closeStruct.ErrorMessage = 5, err.Error()
		return closeStruct
	}

	// Match intitialization, making directory & file Match bufio writer
	if _, err := os.Stat(p.ProcessProp.PathDataOnly); os.IsNotExist(err) {
		fmt.Printf("    > [ Making Data Only Directory ] : ")
		err = os.MkdirAll(p.ProcessProp.PathDataOnly, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			closeStruct.ProcessStatusID, closeStruct.ErrorMessage = 5, err.Error()
			return closeStruct
		}
		fmt.Println("Success")
	}

	// Opening & Creating Data Only File
	outputDataOnly := p.ProcessProp.PathDataOnly + p.ProcessProp.FilenameDataOnly
	fmt.Println("    > [ Data Only File ] :", outputDataOnly)
	fileDataOnly, err := os.Create(outputDataOnly)
	if err != nil {
		fmt.Println("    > Failed, with error =>", err.Error())
		closeStruct.ProcessStatusID, closeStruct.ErrorMessage = 5, err.Error()
		return closeStruct
	}
	// Bufio Writer Data Only File
	writerDataOnly := bufio.NewWriter(fileDataOnly)

	iterDB := dbconn.NewIterator(nil, nil)
	for iterDB.Next() {
		matchKey := iterDB.Key()
		exists, _ := matchDB.Has(matchKey, nil)

		columns := strings.Split(string(iterDB.Value()), "|")
		partnerID := columns[10]
		dur, _ := strconv.Atoi(columns[4])
		revenue, _ := strconv.ParseFloat(columns[7], 64)
		edtm := columns[0]

		if closeStruct.LogPartnerID[partnerID] == nil {
			closeStruct.LogPartnerID[partnerID] = &model.LogMatchContent{}
		}

		if exists == false {
			if !strings.HasPrefix(edtm, p.ProcessProp.Periode) {
				continue
			}

			fmt.Fprintln(writerDataOnly, string(iterDB.Value()))

			closeStruct.LogPartnerID[partnerID].DurationsOnly += dur

			closeStruct.LogPartnerID[partnerID].RevenuesOnly += revenue

			closeStruct.LogPartnerID[partnerID].RecordsOnly++

			closeStruct.CountOnlyRecord++
			closeStruct.CountOnlyDuration += dur
		} else {
			closeStruct.LogPartnerID[partnerID].DurationsMatch += dur

			closeStruct.LogPartnerID[partnerID].RevenuesMatch += revenue

			closeStruct.LogPartnerID[partnerID].RecordsMatch++

			closeStruct.CountMatchRecord++
			closeStruct.CountMatchDuration += dur
		}

		closeStruct.LogPartnerID[partnerID].DurationsInput += dur

		closeStruct.LogPartnerID[partnerID].RevenuesInput += revenue

		closeStruct.LogPartnerID[partnerID].RecordsInput++

		closeStruct.CountInputRecord++
		closeStruct.CountInputDuration += dur
	}
	iterDB.Release()
	fmt.Println("    > [ Total Data DB :", closeStruct.CountInputRecord, ", Total Data Only :", closeStruct.CountOnlyRecord, " records,", closeStruct.CountOnlyDuration, "seconds ]")

	// File Data Only Bufio Flush
	err = writerDataOnly.Flush()
	if err != nil {
		fmt.Println("    > Failed, with error =>", err.Error())
		closeStruct.ProcessStatusID, closeStruct.ErrorMessage = 5, err.Error()
		return closeStruct
	}
	fileDataOnly.Close()

	matchDB.Close()

	dbconn.Close()

	closeStruct.ProcessStatusID = 7

	return closeStruct
}
