package handler

import (
	"fmt"

	"bitbucket.org/billing/go-data-only/model"
)

// Process :
type Process struct{}

var modelProcess model.ProcessDataOnly
var modelProcessReport model.ProcessReport

// GetProcess :
func (p *Process) GetProcess() ([]model.ProcessDataOnly, error) {
	output, err := modelProcess.GetProcess()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return output, nil
}

// UpdateStatus :
func (p *Process) UpdateStatus(processID, processStatusID string) error {
	fmt.Printf("  > [ Update Process Status To : " + processStatusID + " ] : ")
	err := modelProcess.UpdateStatus(processID, processStatusID)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}
	fmt.Println("Success")
	return nil
}

// CloseProcess :
func (p *Process) CloseProcess(closeStruct *model.ClosingStruct) error {
	err := modelProcess.CloseProcess(closeStruct)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	return nil
}

// GetProcessReportDaily :
func (p *Process) GetProcessReportDaily() ([]model.ProcessReport, error) {
	output, err := modelProcessReport.GetProcessReportDaily()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return output, nil
}

// CloseProcessReportDaily :
func (p *Process) CloseProcessReportDaily(closeStruct *model.StructCloseReport) error {
	err := modelProcess.CloseProcessReportDaily(closeStruct)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	return nil
}

// GetLogMatch :
func (p *Process) GetLogMatch(batchID string) ([]model.StructLogMatch, error) {
	output, err := modelProcessReport.GetLogMatch(batchID)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return output, nil
}

// GetLogTapinOnly :
func (p *Process) GetLogTapinOnly(batchID string) ([]model.StructLogTapinOnly, error) {
	output, err := modelProcessReport.GetLogTapinOnly(batchID)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return output, nil
}

// GetProcessReportMonthly :
func (p *Process) GetProcessReportMonthly() ([]model.ProcessReportMonthly, error) {
	output, err := modelProcessReport.GetProcessReportMonthly()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return output, nil
}

// GetLogReconMonthly :
func (p *Process) GetLogReconMonthly(periode, moMt, postPre string) (model.StructLogReconMonthly, error) {
	output, err := modelProcessReport.GetLogReconMonthly(periode, moMt, postPre)
	if err != nil {
		fmt.Println(err.Error())
		return output, err
	}
	return output, nil
}

// GetLogCheckDUp :
func (p *Process) GetLogCheckDUp(periode, moMt, postPre string) (map[string]model.StructLogCekdup, error) {
	output, err := modelProcessReport.GetLogCheckdup(periode, moMt, postPre)
	if err != nil {
		fmt.Println(err.Error())
		return output, err
	}
	return output, nil
}

// GetLogOCSOnly :
func (p *Process) GetLogOCSOnly(periode, moMt, postPre string) (model.StructLogOCSOnly, error) {
	output, err := modelProcessReport.GetLogOCSOnly(periode, moMt, postPre)
	if err != nil {
		fmt.Println(err.Error())
		return output, err
	}
	return output, nil
}

// GetLogRecon :
func (p *Process) GetLogRecon(periode, moMt, postPre string) (map[string]model.StructLogRecon, error) {
	output, err := modelProcessReport.GetLogRecon(periode, moMt, postPre)
	if err != nil {
		fmt.Println(err.Error())
		return output, err
	}
	return output, nil
}

// GetLogMatchToleransiGroup :
func (p *Process) GetLogMatchToleransiGroup(periode, moMt, postPre string) (map[string]model.StructLogMatchToleransiGroup, error) {
	output, err := modelProcessReport.GetLogMatchToleransiGroup(periode, moMt, postPre)
	if err != nil {
		fmt.Println(err.Error())
		return output, err
	}
	return output, nil
}

// GetLogReportMonthly :
func (p *Process) GetLogReportMonthly(periode, moMt, postPre, dataTypeID string) ([]model.StructLogReportMonthly, error) {
	output, err := modelProcessReport.GetLogReportMonthly(periode, moMt, postPre, dataTypeID)
	if err != nil {
		fmt.Println(err.Error())
		return output, err
	}
	return output, nil
}

// GetLogCheckDupPartnerID :
func (p *Process) GetLogCheckDupPartnerID(periode, moMt, postPre, dataType string) ([]model.StructLogCekdupPartnerID, error) {
	output, err := modelProcessReport.GetLogCheckDupPartnerID(periode, moMt, postPre, dataType)
	if err != nil {
		fmt.Println(err.Error())
		return output, err
	}
	return output, nil
}

// GetLogCheckDupTopTen :
func (p *Process) GetLogCheckDupTopTen(periode, moMt, postPre, dataType string) ([]model.StructLogCekdupTopTen, error) {
	output, err := modelProcessReport.GetLogCheckDupTopTen(periode, moMt, postPre, dataType)
	if err != nil {
		fmt.Println(err.Error())
		return output, err
	}
	return output, nil
}

// CloseProcessReportMonthly :
func (p *Process) CloseProcessReportMonthly(closeStruct *model.StructCloseReportMonthly) error {
	err := modelProcess.CloseProcessReportMonthly(closeStruct)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	return nil
}
