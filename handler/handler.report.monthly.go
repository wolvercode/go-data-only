package handler

import (
	"math"
	"strings"

	"github.com/360EntSecGroup-Skylar/excelize"
)

// MakeFirstSheetMonthly :
func (p *ReportMaker) MakeFirstSheetMonthly(f *excelize.File) {
	usedVindex := 0
	// Create a new sheet.
	index := f.NewSheet("Sheet1")
	f.SetSheetName("Sheet1", "Page 1 - Preprocessing Steps")
	f.SetColWidth("Page 1 - Preprocessing Steps", "A", "A", 35)
	f.SetColWidth("Page 1 - Preprocessing Steps", "B", "E", 22)
	f.SetColWidth("Page 1 - Preprocessing Steps", "F", "F", 35)
	f.SetColWidth("Page 1 - Preprocessing Steps", "G", "G", 22)
	f.SetColWidth("Page 1 - Preprocessing Steps", "H", "H", 35)
	f.SetColWidth("Page 1 - Preprocessing Steps", "I", "I", 35)
	f.SetColWidth("Page 1 - Preprocessing Steps", "J", "M", 22)
	f.SetColWidth("Page 1 - Preprocessing Steps", "N", "N", 35)
	f.SetColWidth("Page 1 - Preprocessing Steps", "O", "R", 22)
	f.SetColWidth("Page 1 - Preprocessing Steps", "S", "S", 35)

	mapReconTitle := map[string]map[string]interface{}{
		"A": {
			"Value":     "RECONCILLIATION REPORTS",
			"MergeTill": "E",
			"Styles":    p.MapStyle["partHeaderNoBorder"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapReconTitle)

	mapHeader2 := map[int]map[string]map[string]interface{}{
		1: {
			"A": {
				"Value":  "Recon Call Type",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     "Roaming Voice  -  " + strings.ToUpper(p.ProcessPropMonthly.MoMt) + " Only",
				"MergeTill": "E",
				"Styles":    p.MapStyle["style"],
			},
		},
		2: {
			"A": {
				"Value":  "Recon Runtime",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     p.TimeParse.Format("January 2006"),
				"MergeTill": "E",
				"Styles":    p.MapStyle["style"],
			},
		},
		4: {
			"A": {
				"Value":  "Recon TAPCODE",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     p.ProcessPropMonthly.FilterTapCode,
				"MergeTill": "E",
				"Styles":    p.MapStyle["style"],
			},
		},
		5: {
			"A": {
				"Value":  "PAGE",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     "Page 1 - Preprocessing Steps",
				"MergeTill": "E",
				"Styles":    p.MapStyle["style"],
			},
		},
	}

	excelCustomCmd.SetCellMultiple(f, index, &usedVindex, mapHeader2)

	usedVindex++ // skip one row

	mapTapinProcessingStepsTitle := map[string]map[string]interface{}{
		"A": {
			"Value":     "TAPIN :: File Processing Steps",
			"MergeTill": "B",
			"Styles":    p.MapStyle["partHeaderNoBorder"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingStepsTitle)

	mapTapinProcessingStepsMainHeader := map[string]map[string]interface{}{
		"A": {
			"Value":     "INPUT FILES",
			"MergeTill": "C",
			"Styles":    p.MapStyle["mainHeaderStyleDark"],
		},
		"D": {
			"Value":     "SPLIT MO/MT",
			"MergeTill": "I",
			"Styles":    p.MapStyle["mainHeaderStyleLight"],
		},
		"J": {
			"Value":     "LOOKUP",
			"MergeTill": "N",
			"Styles":    p.MapStyle["mainHeaderStyleDark"],
		},
		"O": {
			"Value":     "CHECK DUPLICATE",
			"MergeTill": "S",
			"Styles":    p.MapStyle["mainHeaderStyleLight"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingStepsMainHeader)

	mapTapinProcessingStepsSubHeader := map[string]map[string]interface{}{
		"A": {
			"Value":  "file_cdr",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "contents",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "record_count",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "split_input",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  "split_reject",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"F": {
			"Value":  "split_mt_output",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"G": {
			"Value":  "split_mo_output",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"H": {
			"Value":  "split_contents",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"I": {
			"Value":  "split_output_file",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"J": {
			"Value":  "lookup_input",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"K": {
			"Value":  "lookup_reject",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"L": {
			"Value":  "lookup_output",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"M": {
			"Value":  "lookup_contents",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"N": {
			"Value":  "lookup_output_files",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"O": {
			"Value":  "cekdup_input",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"P": {
			"Value":  "cekdup_duplicate",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"Q": {
			"Value":  "cekdup_unique",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"R": {
			"Value":  "cekdup_contents",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"S": {
			"Value":  "cekdup_output_files",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingStepsSubHeader)

	mapTotalTAPIN := make(map[string]int)
	for _, field := range *p.LogReportMonthlyTAPIN {
		mapTotalTAPIN["SplitInput"] += field.SplitInput
		mapTotalTAPIN["SplitReject"] += field.SplitReject
		mapTotalTAPIN["SplitMT"] += field.SplitMT
		mapTotalTAPIN["SplitMO"] += field.SplitMO
		mapTotalTAPIN["LookupInput"] += field.LookupInput
		mapTotalTAPIN["LookupReject"] += field.LookupReject
		mapTotalTAPIN["LookupOutput"] += field.LookupOutput
		mapTotalTAPIN["CekdupInput"] += field.CekdupInput
		mapTotalTAPIN["CekdupDuplicate"] += field.CekdupDuplicate
		mapTotalTAPIN["CekdupUnduplicate"] += field.CekdupUnduplicate

		mapTapinProcessingBodyValues := map[string]map[string]interface{}{
			"A": {
				"Value":  field.FilenameCDR,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  field.MoMt,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  field.SplitInput,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  field.SplitInput,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"E": {
				"Value":  field.SplitReject,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  field.SplitMT,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"G": {
				"Value":  field.SplitMO,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"H": {
				"Value":  strings.ToUpper(field.MoMt),
				"Styles": p.MapStyle["bodyStyle"],
			},
			"I": {
				"Value":  field.SplitFilename,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"J": {
				"Value":  field.LookupInput,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"K": {
				"Value":  field.LookupReject,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"L": {
				"Value":  field.LookupOutput,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"M": {
				"Value":  strings.ToUpper(field.MoMt) + ", " + strings.ToUpper(field.PostPre),
				"Styles": p.MapStyle["bodyStyle"],
			},
			"N": {
				"Value":  field.LookupFilename,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"O": {
				"Value":  field.CekdupInput,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"P": {
				"Value":  field.CekdupDuplicate,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"Q": {
				"Value":  field.CekdupUnduplicate,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"R": {
				"Value":  strings.ToUpper(field.MoMt) + ", " + strings.ToUpper(field.PostPre),
				"Styles": p.MapStyle["bodyStyle"],
			},
			"S": {
				"Value":  field.CekdupFilename,
				"Styles": p.MapStyle["bodyStyle"],
			},
		}
		excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingBodyValues)
	}

	mapTapinProcessingStepsFooter := map[string]map[string]interface{}{
		"A": {
			"Value":     "TOTAL",
			"MergeTill": "B",
			"Styles":    p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  mapTotalTAPIN["SplitInput"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"D": {
			"Value":  mapTotalTAPIN["SplitInput"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"E": {
			"Value":  mapTotalTAPIN["SplitReject"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"F": {
			"Value":  mapTotalTAPIN["SplitMT"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"G": {
			"Value":  mapTotalTAPIN["SplitMO"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"H": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"I": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"J": {
			"Value":  mapTotalTAPIN["LookupInput"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"K": {
			"Value":  mapTotalTAPIN["LookupReject"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"L": {
			"Value":  mapTotalTAPIN["LookupOutput"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"M": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"N": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"O": {
			"Value":  mapTotalTAPIN["CekdupInput"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"P": {
			"Value":  mapTotalTAPIN["CekdupDuplicate"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"Q": {
			"Value":  mapTotalTAPIN["CekdupUnduplicate"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"R": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"S": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingStepsFooter)

	usedVindex++ // skip one row

	mapOCSProcessingStepsTitle := map[string]map[string]interface{}{
		"A": {
			"Value":     "TC(CHG) :: File Processing Steps",
			"MergeTill": "B",
			"Styles":    p.MapStyle["partHeaderNoBorder"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapOCSProcessingStepsTitle)

	mapOCSProcessingStepsMainHeader := map[string]map[string]interface{}{
		"A": {
			"Value":     "INPUT FILES",
			"MergeTill": "C",
			"Styles":    p.MapStyle["mainHeaderStyleDark"],
		},
		"D": {
			"Value":     "SPLIT MO/MT",
			"MergeTill": "I",
			"Styles":    p.MapStyle["mainHeaderStyleLight"],
		},
		"J": {
			"Value":     "LOOKUP",
			"MergeTill": "N",
			"Styles":    p.MapStyle["mainHeaderStyleDark"],
		},
		"O": {
			"Value":     "CHECK DUPLICATE",
			"MergeTill": "S",
			"Styles":    p.MapStyle["mainHeaderStyleLight"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapOCSProcessingStepsMainHeader)

	mapOCSProcessingStepsSubHeader := map[string]map[string]interface{}{
		"A": {
			"Value":  "file_cdr",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "contents",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "record_count",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "split_input",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  "split_reject",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"F": {
			"Value":  "split_mt_output",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"G": {
			"Value":  "split_mo_output",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"H": {
			"Value":  "split_contents",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"I": {
			"Value":  "split_output_file",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"J": {
			"Value":  "lookup_input",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"K": {
			"Value":  "lookup_reject",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"L": {
			"Value":  "lookup_output",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"M": {
			"Value":  "lookup_contents",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"N": {
			"Value":  "lookup_output_files",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"O": {
			"Value":  "cekdup_input",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"P": {
			"Value":  "cekdup_duplicate",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"Q": {
			"Value":  "cekdup_unique",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"R": {
			"Value":  "cekdup_contents",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"S": {
			"Value":  "cekdup_output_files",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapOCSProcessingStepsSubHeader)

	mapTotalOCS := make(map[string]int)
	for _, field := range *p.LogReportMonthlyOCS {
		mapTotalOCS["SplitInput"] += field.SplitInput
		mapTotalOCS["SplitReject"] += field.SplitReject
		mapTotalOCS["SplitMT"] += field.SplitMT
		mapTotalOCS["SplitMO"] += field.SplitMO
		mapTotalOCS["LookupInput"] += field.LookupInput
		mapTotalOCS["LookupReject"] += field.LookupReject
		mapTotalOCS["LookupOutput"] += field.LookupOutput
		mapTotalOCS["CekdupInput"] += field.CekdupInput
		mapTotalOCS["CekdupDuplicate"] += field.CekdupDuplicate
		mapTotalOCS["CekdupUnduplicate"] += field.CekdupUnduplicate

		mapOCSProcessingBodyValues := map[string]map[string]interface{}{
			"A": {
				"Value":  field.FilenameCDR,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  field.MoMt,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  field.SplitInput,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  field.SplitInput,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"E": {
				"Value":  field.SplitReject,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  field.SplitMT,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"G": {
				"Value":  field.SplitMO,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"H": {
				"Value":  strings.ToUpper(field.MoMt),
				"Styles": p.MapStyle["bodyStyle"],
			},
			"I": {
				"Value":  field.SplitFilename,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"J": {
				"Value":  field.LookupInput,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"K": {
				"Value":  field.LookupReject,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"L": {
				"Value":  field.LookupOutput,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"M": {
				"Value":  strings.ToUpper(field.MoMt) + ", " + strings.ToUpper(field.PostPre),
				"Styles": p.MapStyle["bodyStyle"],
			},
			"N": {
				"Value":  field.LookupFilename,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"O": {
				"Value":  field.CekdupInput,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"P": {
				"Value":  field.CekdupDuplicate,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"Q": {
				"Value":  field.CekdupUnduplicate,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"R": {
				"Value":  strings.ToUpper(field.MoMt) + ", " + strings.ToUpper(field.PostPre),
				"Styles": p.MapStyle["bodyStyle"],
			},
			"S": {
				"Value":  field.CekdupFilename,
				"Styles": p.MapStyle["bodyStyle"],
			},
		}
		excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapOCSProcessingBodyValues)
	}

	mapOCSProcessingStepsFooter := map[string]map[string]interface{}{
		"A": {
			"Value":     "TOTAL",
			"MergeTill": "B",
			"Styles":    p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  mapTotalOCS["SplitInput"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"D": {
			"Value":  mapTotalOCS["SplitInput"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"E": {
			"Value":  mapTotalOCS["SplitReject"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"F": {
			"Value":  mapTotalOCS["SplitMT"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"G": {
			"Value":  mapTotalOCS["SplitMO"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"H": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"I": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"J": {
			"Value":  mapTotalOCS["LookupInput"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"K": {
			"Value":  mapTotalOCS["LookupReject"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"L": {
			"Value":  mapTotalOCS["LookupOutput"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"M": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"N": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"O": {
			"Value":  mapTotalOCS["CekdupInput"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"P": {
			"Value":  mapTotalOCS["CekdupDuplicate"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"Q": {
			"Value":  mapTotalOCS["CekdupUnduplicate"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"R": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"S": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapOCSProcessingStepsFooter)

	usedVindex++ // skip one row

	mapProcessingStatisticsTitle := map[string]map[string]interface{}{
		"A": {
			"Value":     "File Processing Steps :: Statistics",
			"MergeTill": "B",
			"Styles":    p.MapStyle["partHeaderNoBorder"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapProcessingStatisticsTitle)

	usedVindex++ // skip one row

	mapTapinProcessingStatisticsTitle := map[string]map[string]interface{}{
		"A": {
			"Value":     "TAPIN Statistics",
			"MergeTill": "B",
			"Styles":    p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingStatisticsTitle)

	mapTapinProcessingStatisticsMainHeader := map[string]map[string]interface{}{
		"A": {
			"Value":     "SPLIT MO/MT",
			"MergeTill": "B",
			"Styles":    p.MapStyle["mainHeaderStyleDark"],
		},
		"C": {
			"Value":     "LOOKUP",
			"MergeTill": "D",
			"Styles":    p.MapStyle["mainHeaderStyleLight"],
		},
		"E": {
			"Value":     "CHECK DUPLICATE",
			"MergeTill": "F",
			"Styles":    p.MapStyle["mainHeaderStyleDark"],
		},
	}

	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingStatisticsMainHeader)

	mapTapinProcessingStatisticsSubHeader := map[string]map[string]interface{}{
		"A": {
			"Value":  "Criteria",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "%",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "Criteria",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "%",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  "Criteria",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"F": {
			"Value":  "%",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}

	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinProcessingStatisticsSubHeader)

	mapTapinProcessingStatisticsBody := map[int]map[string]map[string]interface{}{
		1: {
			"A": {
				"Value":  "Reject",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  (float32(mapTotalTAPIN["SplitReject"]) / float32(mapTotalTAPIN["SplitInput"])),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
			"C": {
				"Value":  "Reject",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  (float32(mapTotalTAPIN["LookupReject"]) / float32(mapTotalTAPIN["LookupInput"])),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
			"E": {
				"Value":  "Duplicate",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  (float32(mapTotalTAPIN["CekdupDuplicate"]) / float32(mapTotalTAPIN["CekdupInput"])),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
		},
		2: {
			"A": {
				"Value":  "MT",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  (float32(mapTotalTAPIN["SplitMT"]) / float32(mapTotalTAPIN["SplitInput"])),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
			"C": {
				"Value":  "Output",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  (float32(mapTotalTAPIN["LookupOutput"]) / float32(mapTotalTAPIN["LookupInput"])),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
			"E": {
				"Value":  "Unduplicate",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  (float32(mapTotalTAPIN["CekdupUnduplicate"]) / float32(mapTotalTAPIN["CekdupInput"])),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
		},
		3: {
			"A": {
				"Value":  "MO",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  (float32(mapTotalTAPIN["SplitMO"]) / float32(mapTotalTAPIN["SplitInput"])),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
		},
	}
	excelCustomCmd.SetCellMultiple(f, index, &usedVindex, mapTapinProcessingStatisticsBody)

	usedVindex++ // skip one row

	mapOCSProcessingStatisticsTitle := map[string]map[string]interface{}{
		"A": {
			"Value":     "TC(CHG) Statistics",
			"MergeTill": "B",
			"Styles":    p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapOCSProcessingStatisticsTitle)

	mapOCSProcessingStatisticsMainHeader := map[string]map[string]interface{}{
		"A": {
			"Value":     "SPLIT MO/MT",
			"MergeTill": "B",
			"Styles":    p.MapStyle["mainHeaderStyleDark"],
		},
		"C": {
			"Value":     "LOOKUP",
			"MergeTill": "D",
			"Styles":    p.MapStyle["mainHeaderStyleLight"],
		},
		"E": {
			"Value":     "CHECK DUPLICATE",
			"MergeTill": "F",
			"Styles":    p.MapStyle["mainHeaderStyleDark"],
		},
	}

	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapOCSProcessingStatisticsMainHeader)

	mapOCSProcessingStatisticsSubHeader := map[string]map[string]interface{}{
		"A": {
			"Value":  "Criteria",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "%",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "Criteria",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "%",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  "Criteria",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"F": {
			"Value":  "%",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}

	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapOCSProcessingStatisticsSubHeader)

	mapOCSProcessingStatisticsBody := map[int]map[string]map[string]interface{}{
		1: {
			"A": {
				"Value":  "Reject",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  (float32(mapTotalOCS["SplitReject"]) / float32(mapTotalOCS["SplitInput"])),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
			"C": {
				"Value":  "Reject",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  (float32(mapTotalOCS["LookupReject"]) / float32(mapTotalOCS["LookupInput"])),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
			"E": {
				"Value":  "Duplicate",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  (float32(mapTotalOCS["CekdupDuplicate"]) / float32(mapTotalOCS["CekdupInput"])),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
		},
		2: {
			"A": {
				"Value":  "MT",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  (float32(mapTotalOCS["SplitMT"]) / float32(mapTotalOCS["SplitInput"])),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
			"C": {
				"Value":  "Output",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  (float32(mapTotalOCS["LookupOutput"]) / float32(mapTotalOCS["LookupInput"])),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
			"E": {
				"Value":  "Unduplicate",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  (float32(mapTotalOCS["CekdupUnduplicate"]) / float32(mapTotalOCS["CekdupInput"])),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
		},
		3: {
			"A": {
				"Value":  "MO",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  (float32(mapTotalOCS["SplitMO"]) / float32(mapTotalOCS["SplitInput"])),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
		},
	}
	excelCustomCmd.SetCellMultiple(f, index, &usedVindex, mapOCSProcessingStatisticsBody)

	usedVindex++ // skip one row

	mapLookupRejectTitle := map[string]map[string]interface{}{
		"A": map[string]interface{}{
			"Value":     "LOOKUP REJECT DETAILS",
			"MergeTill": "B",
			"Styles":    p.MapStyle["partHeaderNoBorder"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapLookupRejectTitle)

	usedVindex++ // skip one row

	mapLookupRejectTitleTAPIN := map[string]map[string]interface{}{
		"A": map[string]interface{}{
			"Value":     "Reject Detail for TAPIN (record)",
			"MergeTill": "B",
			"Styles":    p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapLookupRejectTitleTAPIN)

	mapDetailRejectLookupHeaderTAPIN := map[string]map[string]interface{}{
		"A": {
			"Value":  "lookup_input_file",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "reject_total",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "field_empty",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "durasi_0",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  "tapcode_notfound",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"F": {
			"Value":  "imsi_notfound",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"G": {
			"Value":  "partner_id_notallowed",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"H": {
			"Value":  "lookup_reject_output_file",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDetailRejectLookupHeaderTAPIN)

	mapRejectTotalTAPIN := make(map[string]int)
	for _, field := range *p.LogReportMonthlyTAPIN {
		mapRejectTotalTAPIN["LookupReject"] += field.LookupReject
		mapRejectTotalTAPIN["RejectFieldEmpty"] += field.RejectFieldEmpty
		mapRejectTotalTAPIN["RejectDurasi0"] += field.RejectDurasi0
		mapRejectTotalTAPIN["RejectTapCode"] += field.RejectTapCode
		mapRejectTotalTAPIN["RejectImsi"] += field.RejectImsi
		mapRejectTotalTAPIN["RejectMOPartnerID"] += field.RejectMOPartnerID
		rejectName := "-"
		if field.LookupRejectFilename != "" {
			rejectName = field.LookupRejectFilename
		}

		mapTapinDetailRejectLookupBody := map[string]map[string]interface{}{
			"A": {
				"Value":  field.LookupFilename,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  field.LookupReject,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  field.RejectFieldEmpty,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  field.RejectDurasi0,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"E": {
				"Value":  field.RejectTapCode,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  field.RejectImsi,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"G": {
				"Value":  field.RejectMOPartnerID,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"H": {
				"Value":  rejectName,
				"Styles": p.MapStyle["bodyStyle"],
			},
		}
		excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinDetailRejectLookupBody)
	}

	mapDetailRejectLookupTotalTAPIN := map[string]map[string]interface{}{
		"A": {
			"Value":  "TOTAL",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  mapRejectTotalTAPIN["LookupReject"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"C": {
			"Value":  mapRejectTotalTAPIN["RejectFieldEmpty"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"D": {
			"Value":  mapRejectTotalTAPIN["RejectDurasi0"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"E": {
			"Value":  mapRejectTotalTAPIN["RejectTapCode"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"F": {
			"Value":  mapRejectTotalTAPIN["RejectImsi"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"G": {
			"Value":  mapRejectTotalTAPIN["RejectMOPartnerID"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"H": {
			"Value":  "",
			"Styles": p.MapStyle["footerStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDetailRejectLookupTotalTAPIN)

	var percentRejectFieldEmptyTAPIN float32
	var percentDurasi0TAPIN float32
	var percentRejectTapCodeTAPIN float32
	var percentRejectImsiTAPIN float32
	var percentRejectMOPartnerIDTAPIN float32

	if mapRejectTotalTAPIN["LookupReject"] != 0 {
		percentRejectFieldEmptyTAPIN = float32(mapRejectTotalTAPIN["RejectFieldEmpty"]) / float32(mapRejectTotalTAPIN["LookupReject"])
		percentDurasi0TAPIN = float32(mapRejectTotalTAPIN["RejectDurasi0"]) / float32(mapRejectTotalTAPIN["LookupReject"])
		percentRejectTapCodeTAPIN = float32(mapRejectTotalTAPIN["RejectTapCode"]) / float32(mapRejectTotalTAPIN["LookupReject"])
		percentRejectImsiTAPIN = float32(mapRejectTotalTAPIN["RejectImsi"]) / float32(mapRejectTotalTAPIN["LookupReject"])
		percentRejectMOPartnerIDTAPIN = float32(mapRejectTotalTAPIN["RejectMOPartnerID"]) / float32(mapRejectTotalTAPIN["LookupReject"])
	}

	mapDetailRejectLookupPercentageTAPIN := map[string]map[string]interface{}{
		"A": {
			"Value":  "Percentage",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "-",
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"C": {
			"Value":  percentRejectFieldEmptyTAPIN,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"D": {
			"Value":  percentDurasi0TAPIN,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"E": {
			"Value":  percentRejectTapCodeTAPIN,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"F": {
			"Value":  percentRejectImsiTAPIN,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"G": {
			"Value":  percentRejectMOPartnerIDTAPIN,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"H": {
			"Value":  "",
			"Styles": p.MapStyle["footerStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDetailRejectLookupPercentageTAPIN)

	usedVindex++ // skip one row

	mapLookupRejectDurationTitleTAPIN := map[string]map[string]interface{}{
		"A": map[string]interface{}{
			"Value":     "Reject Detail for TAPIN (duration)",
			"MergeTill": "B",
			"Styles":    p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapLookupRejectDurationTitleTAPIN)

	mapDetailRejectDurationLookupHeaderTAPIN := map[string]map[string]interface{}{
		"A": {
			"Value":  "lookup_input_file",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "reject_total",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "tapcode_notfound",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "imsi_notfound",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  "partner_id_notallowed",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"F": {
			"Value":  "lookup_reject_output_file",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDetailRejectDurationLookupHeaderTAPIN)

	mapRejectDurationTotalTAPIN := make(map[string]int)
	for _, field := range *p.LogReportMonthlyTAPIN {
		rejectDurationTotal := field.RejectTapCodeDurasi + field.RejectImsiDurasi + field.RejectMOPartnerIDDurasi

		mapRejectDurationTotalTAPIN["LookupRejectDuration"] += rejectDurationTotal
		mapRejectDurationTotalTAPIN["RejectTapCodeDuration"] += field.RejectTapCodeDurasi
		mapRejectDurationTotalTAPIN["RejectImsiDuration"] += field.RejectImsiDurasi
		mapRejectDurationTotalTAPIN["RejectMOPartnerIDDuration"] += field.RejectMOPartnerIDDurasi
		rejectName := "-"
		if field.LookupRejectFilename != "" {
			rejectName = field.LookupRejectFilename
		}

		mapTapinDetailRejectLookupBody := map[string]map[string]interface{}{
			"A": {
				"Value":  field.LookupFilename,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  rejectDurationTotal,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  field.RejectTapCodeDurasi,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  field.RejectImsiDurasi,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"E": {
				"Value":  field.RejectMOPartnerIDDurasi,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  rejectName,
				"Styles": p.MapStyle["bodyStyle"],
			},
		}
		excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinDetailRejectLookupBody)
	}

	mapDetailRejectDurationLookupTotalTAPIN := map[string]map[string]interface{}{
		"A": {
			"Value":  "TOTAL",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  mapRejectDurationTotalTAPIN["LookupRejectDuration"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"C": {
			"Value":  mapRejectDurationTotalTAPIN["RejectTapCodeDuration"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"D": {
			"Value":  mapRejectDurationTotalTAPIN["RejectImsiDuration"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"E": {
			"Value":  mapRejectDurationTotalTAPIN["RejectMOPartnerIDDuration"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"F": {
			"Value":  "",
			"Styles": p.MapStyle["footerStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDetailRejectDurationLookupTotalTAPIN)

	var percentRejectDurationTapCodeTAPIN float32
	var percentRejectDurationImsiTAPIN float32
	var percentRejectDurationMOPartnerIDTAPIN float32

	if mapRejectDurationTotalTAPIN["LookupRejectDuration"] != 0 {
		percentRejectDurationTapCodeTAPIN = float32(mapRejectDurationTotalTAPIN["RejectTapCodeDuration"]) / float32(mapRejectDurationTotalTAPIN["LookupRejectDuration"])
		percentRejectDurationImsiTAPIN = float32(mapRejectDurationTotalTAPIN["RejectImsiDuration"]) / float32(mapRejectDurationTotalTAPIN["LookupRejectDuration"])
		percentRejectDurationMOPartnerIDTAPIN = float32(mapRejectDurationTotalTAPIN["RejectMOPartnerIDDuration"]) / float32(mapRejectDurationTotalTAPIN["LookupRejectDuration"])
	}

	mapDetailRejectDurationLookupPercentageTAPIN := map[string]map[string]interface{}{
		"A": {
			"Value":  "Percentage",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "-",
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"C": {
			"Value":  percentRejectDurationTapCodeTAPIN,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"D": {
			"Value":  percentRejectDurationImsiTAPIN,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"E": {
			"Value":  percentRejectDurationMOPartnerIDTAPIN,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"F": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDetailRejectDurationLookupPercentageTAPIN)

	usedVindex++ // skip one row

	mapLookupRejectTitleOCS := map[string]map[string]interface{}{
		"A": map[string]interface{}{
			"Value":     "Reject Detail for TC(CHG) (record)",
			"MergeTill": "B",
			"Styles":    p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapLookupRejectTitleOCS)

	mapDetailRejectLookupHeaderOCS := map[string]map[string]interface{}{
		"A": {
			"Value":  "lookup_input_file",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "reject_total",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "field_empty",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "durasi_0",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  "tapcode_notfound",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"F": {
			"Value":  "imsi_notfound",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"G": {
			"Value":  "partner_id_notallowed",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"H": {
			"Value":  "lookup_reject_output_file",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDetailRejectLookupHeaderOCS)

	mapRejectTotalOCS := make(map[string]int)
	for _, field := range *p.LogReportMonthlyOCS {
		mapRejectTotalOCS["LookupReject"] += field.LookupReject
		mapRejectTotalOCS["RejectFieldEmpty"] += field.RejectFieldEmpty
		mapRejectTotalOCS["RejectDurasi0"] += field.RejectDurasi0
		mapRejectTotalOCS["RejectTapCode"] += field.RejectTapCode
		mapRejectTotalOCS["RejectImsi"] += field.RejectImsi
		mapRejectTotalOCS["RejectMOPartnerID"] += field.RejectMOPartnerID
		rejectName := "-"
		if field.LookupRejectFilename != "" {
			rejectName = field.LookupRejectFilename
		}

		mapTapinDetailRejectLookupBody := map[string]map[string]interface{}{
			"A": {
				"Value":  field.LookupFilename,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  field.LookupReject,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  field.RejectFieldEmpty,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  field.RejectDurasi0,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"E": {
				"Value":  field.RejectTapCode,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  field.RejectImsi,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"G": {
				"Value":  field.RejectMOPartnerID,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"H": {
				"Value":  rejectName,
				"Styles": p.MapStyle["bodyStyle"],
			},
		}
		excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinDetailRejectLookupBody)
	}

	mapDetailRejectLookupTotalOCS := map[string]map[string]interface{}{
		"A": {
			"Value":  "TOTAL",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  mapRejectTotalOCS["LookupReject"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"C": {
			"Value":  mapRejectTotalOCS["RejectFieldEmpty"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"D": {
			"Value":  mapRejectTotalOCS["RejectDurasi0"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"E": {
			"Value":  mapRejectTotalOCS["RejectTapCode"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"F": {
			"Value":  mapRejectTotalOCS["RejectImsi"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"G": {
			"Value":  mapRejectTotalOCS["RejectMOPartnerID"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"H": {
			"Value":  "",
			"Styles": p.MapStyle["footerStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDetailRejectLookupTotalOCS)

	var percentRejectFieldEmptyOCS float32
	var percentDurasi0OCS float32
	var percentRejectTapCodeOCS float32
	var percentRejectImsiOCS float32
	var percentRejectMOPartnerIDOCS float32

	if mapRejectTotalOCS["LookupReject"] != 0 {
		percentRejectFieldEmptyOCS = float32(mapRejectTotalOCS["RejectFieldEmpty"]) / float32(mapRejectTotalOCS["LookupReject"])
		percentDurasi0OCS = float32(mapRejectTotalOCS["RejectDurasi0"]) / float32(mapRejectTotalOCS["LookupReject"])
		percentRejectTapCodeOCS = float32(mapRejectTotalOCS["RejectTapCode"]) / float32(mapRejectTotalOCS["LookupReject"])
		percentRejectImsiOCS = float32(mapRejectTotalOCS["RejectImsi"]) / float32(mapRejectTotalOCS["LookupReject"])
		percentRejectMOPartnerIDOCS = float32(mapRejectTotalOCS["RejectMOPartnerID"]) / float32(mapRejectTotalOCS["LookupReject"])
	}

	mapDetailRejectLookupPercentageOCS := map[string]map[string]interface{}{
		"A": {
			"Value":  "Percentage",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "-",
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"C": {
			"Value":  percentRejectFieldEmptyOCS,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"D": {
			"Value":  percentDurasi0OCS,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"E": {
			"Value":  percentRejectTapCodeOCS,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"F": {
			"Value":  percentRejectImsiOCS,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"G": {
			"Value":  percentRejectMOPartnerIDOCS,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"H": {
			"Value":  "",
			"Styles": p.MapStyle["footerStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDetailRejectLookupPercentageOCS)

	usedVindex++ // skip one row

	mapLookupRejectDurationTitleOCS := map[string]map[string]interface{}{
		"A": map[string]interface{}{
			"Value":     "Reject Detail for TC(CHG) (duration)",
			"MergeTill": "B",
			"Styles":    p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapLookupRejectDurationTitleOCS)

	mapDetailRejectDurationLookupHeaderOCS := map[string]map[string]interface{}{
		"A": {
			"Value":  "lookup_input_file",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "reject_total",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "tapcode_notfound",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "msisdn_notfound",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  "partner_id_notallowed",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"F": {
			"Value":  "lookup_reject_output_file",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDetailRejectDurationLookupHeaderOCS)

	mapRejectDurationTotalOCS := make(map[string]int)
	for _, field := range *p.LogReportMonthlyOCS {
		rejectDurationTotal := field.RejectTapCodeDurasi + field.RejectImsiDurasi + field.RejectMOPartnerIDDurasi

		mapRejectDurationTotalOCS["LookupRejectDuration"] += rejectDurationTotal
		mapRejectDurationTotalOCS["RejectTapCodeDuration"] += field.RejectTapCodeDurasi
		mapRejectDurationTotalOCS["RejectImsiDuration"] += field.RejectImsiDurasi
		mapRejectDurationTotalOCS["RejectMOPartnerIDDuration"] += field.RejectMOPartnerIDDurasi
		rejectName := "-"
		if field.LookupRejectFilename != "" {
			rejectName = field.LookupRejectFilename
		}

		mapTapinDetailRejectLookupBody := map[string]map[string]interface{}{
			"A": {
				"Value":  field.LookupFilename,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  rejectDurationTotal,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  field.RejectTapCodeDurasi,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  field.RejectImsiDurasi,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"E": {
				"Value":  field.RejectMOPartnerIDDurasi,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  rejectName,
				"Styles": p.MapStyle["bodyStyle"],
			},
		}
		excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapTapinDetailRejectLookupBody)
	}

	mapDetailRejectDurationLookupTotalOCS := map[string]map[string]interface{}{
		"A": {
			"Value":  "TOTAL",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  mapRejectDurationTotalOCS["LookupRejectDuration"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"C": {
			"Value":  mapRejectDurationTotalOCS["RejectTapCodeDuration"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"D": {
			"Value":  mapRejectDurationTotalOCS["RejectImsiDuration"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"E": {
			"Value":  mapRejectDurationTotalOCS["RejectMOPartnerIDDuration"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"F": {
			"Value":  "",
			"Styles": p.MapStyle["footerStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDetailRejectDurationLookupTotalOCS)

	var percentRejectDurationTapCodeOCS float32
	var percentRejectDurationImsiOCS float32
	var percentRejectDurationMOPartnerIDOCS float32

	if mapRejectDurationTotalOCS["LookupRejectDuration"] != 0 {
		percentRejectDurationTapCodeOCS = float32(mapRejectDurationTotalOCS["RejectTapCodeDuration"]) / float32(mapRejectDurationTotalOCS["LookupRejectDuration"])
		percentRejectDurationImsiOCS = float32(mapRejectDurationTotalOCS["RejectImsiDuration"]) / float32(mapRejectDurationTotalOCS["LookupRejectDuration"])
		percentRejectDurationMOPartnerIDOCS = float32(mapRejectDurationTotalOCS["RejectMOPartnerIDDuration"]) / float32(mapRejectDurationTotalOCS["LookupRejectDuration"])
	}

	mapDetailRejectDurationLookupPercentageOCS := map[string]map[string]interface{}{
		"A": {
			"Value":  "Percentage",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "-",
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"C": {
			"Value":  percentRejectDurationTapCodeOCS,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"D": {
			"Value":  percentRejectDurationImsiOCS,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"E": {
			"Value":  percentRejectDurationMOPartnerIDOCS,
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
		"F": {
			"Value":  "",
			"Styles": p.MapStyle["subHeaderStylePercentage"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDetailRejectDurationLookupPercentageOCS)

	usedVindex++ // skip one row

	mapDuplicateTitle := map[string]map[string]interface{}{
		"A": map[string]interface{}{
			"Value":     "REMOVE DUPLICATE DETAILS",
			"MergeTill": "B",
			"Styles":    p.MapStyle["partHeaderNoBorder"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateTitle)

	usedVindex++ // skip one row

	mapDuplicateTitleTAPIN := map[string]map[string]interface{}{
		"A": map[string]interface{}{
			"Value":     "Remove Duplicate Detail for TAPIN - by TAPCODE",
			"MergeTill": "B",
			"Styles":    p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateTitleTAPIN)

	mapDuplicateHeaderTAPIN := map[string]map[string]interface{}{
		"A": {
			"Value":  "tapcode",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "record_count",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "sum_duration",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateHeaderTAPIN)

	mapTotalDupTAPIN := make(map[string]int)
	for _, field := range *p.LogCekdupPartnerIDTAPIN {
		mapTotalDupTAPIN["Duplicate"] += field.Duplicate
		mapTotalDupTAPIN["DuplicateDuration"] += field.DuplicateDuration

		mapDuplicateBodyTAPIN := map[string]map[string]interface{}{
			"A": {
				"Value":  field.PartnerID,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  field.Duplicate,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  field.DuplicateDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
		}
		excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateBodyTAPIN)
	}

	mapDuplicateFooterTAPIN := map[string]map[string]interface{}{
		"A": {
			"Value":  "TOTAL",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  mapTotalDupTAPIN["Duplicate"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"C": {
			"Value":  mapTotalDupTAPIN["DuplicateDuration"],
			"Styles": p.MapStyle["footerStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateFooterTAPIN)

	usedVindex++ // skip one row

	mapDuplicateTopTenTitleTAPIN := map[string]map[string]interface{}{
		"A": map[string]interface{}{
			"Value":     "Remove Duplicate Detail for TAPIN - Top 10",
			"MergeTill": "B",
			"Styles":    p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateTopTenTitleTAPIN)

	mapDuplicateTopTenHeaderTAPIN := map[string]map[string]interface{}{
		"A": {
			"Value":  "imsi_a#",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "msisdn_b#",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "tapcode",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "call_date",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  "record_count",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"F": {
			"Value":  "sum_duration",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateTopTenHeaderTAPIN)

	mapTotalDupTopTenTAPIN := make(map[string]int)
	for _, field := range *p.LogCekdupTopTenTAPIN {
		mapTotalDupTopTenTAPIN["RecordCount"] += field.RecordCount
		mapTotalDupTopTenTAPIN["SumDuration"] += field.SumDuration

		mapDuplicateTopTenBodyTAPIN := map[string]map[string]interface{}{
			"A": {
				"Value":  field.Imsi,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  field.BNumber,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  field.PartnerID,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  field.Starttime,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"E": {
				"Value":  field.RecordCount,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  field.SumDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
		}
		excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateTopTenBodyTAPIN)
	}

	mapDuplicateTopTenFooterTAPIN := map[string]map[string]interface{}{
		"A": {
			"Value":  "TOTAL",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "-",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "-",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "-",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  mapTotalDupTopTenTAPIN["RecordCount"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"F": {
			"Value":  mapTotalDupTopTenTAPIN["SumDuration"],
			"Styles": p.MapStyle["footerStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateTopTenFooterTAPIN)

	usedVindex++ // skip one row

	mapDuplicateTitleOCS := map[string]map[string]interface{}{
		"A": map[string]interface{}{
			"Value":     "Remove Duplicate Detail for TC(CHG) - by TAPCODE",
			"MergeTill": "B",
			"Styles":    p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateTitleOCS)

	mapDuplicateHeaderOCS := map[string]map[string]interface{}{
		"A": {
			"Value":  "tapcode",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "record_count",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "sum_duration",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateHeaderOCS)

	mapTotalDupOCS := make(map[string]int)
	for _, field := range *p.LogCekdupPartnerIDOCS {
		mapTotalDupOCS["Duplicate"] += field.Duplicate
		mapTotalDupOCS["DuplicateDuration"] += field.DuplicateDuration

		mapDuplicateBodyOCS := map[string]map[string]interface{}{
			"A": {
				"Value":  field.PartnerID,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  field.Duplicate,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  field.DuplicateDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
		}
		excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateBodyOCS)
	}

	mapDuplicateFooterOCS := map[string]map[string]interface{}{
		"A": {
			"Value":  "TOTAL",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  mapTotalDupOCS["Duplicate"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"C": {
			"Value":  mapTotalDupOCS["DuplicateDuration"],
			"Styles": p.MapStyle["footerStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateFooterOCS)

	usedVindex++ // skip one row

	mapDuplicateTopTenTitleOCS := map[string]map[string]interface{}{
		"A": map[string]interface{}{
			"Value":     "Remove Duplicate Detail for TC(CHG) - Top 10",
			"MergeTill": "B",
			"Styles":    p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateTopTenTitleOCS)

	mapDuplicateTopTenHeaderOCS := map[string]map[string]interface{}{
		"A": {
			"Value":  "imsi_a#",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "msisdn_b#",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "tapcode",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "call_date",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  "record_count",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"F": {
			"Value":  "sum_duration",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateTopTenHeaderOCS)

	mapTotalDupTopTenOCS := make(map[string]int)
	for _, field := range *p.LogCekdupTopTenOCS {
		mapTotalDupTopTenOCS["RecordCount"] += field.RecordCount
		mapTotalDupTopTenOCS["SumDuration"] += field.SumDuration

		mapDuplicateTopTenBodyTAPIN := map[string]map[string]interface{}{
			"A": {
				"Value":  field.Imsi,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  field.BNumber,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  field.PartnerID,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  field.Starttime,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"E": {
				"Value":  field.RecordCount,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  field.SumDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
		}
		excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateTopTenBodyTAPIN)
	}

	mapDuplicateTopTenFooterOCS := map[string]map[string]interface{}{
		"A": {
			"Value":  "TOTAL",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"B": {
			"Value":  "-",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"C": {
			"Value":  "-",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"D": {
			"Value":  "-",
			"Styles": p.MapStyle["subHeaderStyle"],
		},
		"E": {
			"Value":  mapTotalDupTopTenOCS["RecordCount"],
			"Styles": p.MapStyle["footerStyle"],
		},
		"F": {
			"Value":  mapTotalDupTopTenOCS["SumDuration"],
			"Styles": p.MapStyle["footerStyle"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDuplicateTopTenFooterOCS)

}

// MakeSecondSheet :
func (p *ReportMaker) MakeSecondSheet(f *excelize.File) {
	usedVindex := 0
	// Create a new sheet.
	index := f.NewSheet("Sheet2")
	f.SetSheetName("Sheet2", "Page 2 - Summary Result L1")
	f.SetColWidth("Page 2 - Summary Result L1", "A", "A", 15)
	f.SetColWidth("Page 2 - Summary Result L1", "B", "B", 15)
	f.SetColWidth("Page 2 - Summary Result L1", "C", "D", 15)
	f.SetColWidth("Page 2 - Summary Result L1", "E", "E", 15)
	f.SetColWidth("Page 2 - Summary Result L1", "F", "G", 15)
	f.SetColWidth("Page 2 - Summary Result L1", "H", "H", 15)
	f.SetColWidth("Page 2 - Summary Result L1", "I", "S", 15)
	f.SetColWidth("Page 2 - Summary Result L1", "T", "T", 15)

	mapReconTitle := map[string]map[string]interface{}{
		"A": {
			"Value":     "RECONCILLIATION REPORTS",
			"MergeTill": "E",
			"Styles":    p.MapStyle["partHeaderNoBorder"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapReconTitle)

	mapHeader2 := map[int]map[string]map[string]interface{}{
		1: {
			"A": {
				"Value":  "Recon Call Type",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     "Roaming Voice  -  " + strings.ToUpper(p.ProcessPropMonthly.MoMt) + " Only",
				"MergeTill": "E",
				"Styles":    p.MapStyle["style"],
			},
		},
		2: {
			"A": {
				"Value":  "Recon Runtime",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     p.TimeParse.Format("January 2006"),
				"MergeTill": "E",
				"Styles":    p.MapStyle["style"],
			},
		},
		4: {
			"A": {
				"Value":  "Recon TAPCODE",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     p.ProcessPropMonthly.FilterTapCode,
				"MergeTill": "E",
				"Styles":    p.MapStyle["style"],
			},
		},
		5: {
			"A": {
				"Value":  "PAGE",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     "Page 2 - Summary Result L1",
				"MergeTill": "E",
				"Styles":    p.MapStyle["style"],
			},
		},
	}

	excelCustomCmd.SetCellMultiple(f, index, &usedVindex, mapHeader2)

	usedVindex++ // skip one row

	mapDataL1Header := map[string]map[string]interface{}{
		"A": {
			"Value":     "TAPIN :: DATA L1 - SUMMARY",
			"MergeTill": "B",
			"Styles":    p.MapStyle["partHeaderNoBorder"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDataL1Header)

	usedVindex++ // skip one row

	mapDataL1SummaryHeader := map[string]map[string]interface{}{
		"A": {
			"Value":  "SUMMARY",
			"Styles": p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDataL1SummaryHeader)

	mapDataL1SummarySubHeader := map[int]map[string]map[string]interface{}{
		1: {
			"A": {
				"Value":     "ALL TAPCODE",
				"MergeDown": 1,
				"Styles":    p.MapStyle["subHeaderStyleYellow"],
			},
			"B": {
				"Value":     "INPUT DATA",
				"MergeDown": 1,
				"Styles":    p.MapStyle["subHeaderStyleYellow"],
			},
			"C": {
				"Value":     "DUPLICATE",
				"MergeDown": 1,
				"Styles":    p.MapStyle["subHeaderStyleYellow"],
			},
			"D": {
				"Value":     "REJECT",
				"MergeDown": 1,
				"Styles":    p.MapStyle["subHeaderStyleYellow"],
			},
			"E": {
				"Value":     "PROCESS",
				"MergeDown": 1,
				"Styles":    p.MapStyle["subHeaderStyleYellow"],
			},
			"F": {
				"Value":     "Result Match",
				"MergeTill": "H",
				"Styles":    p.MapStyle["subHeaderStyle"],
			},
			"I": {
				"Value":     "Result Oneside",
				"MergeTill": "J",
				"Styles":    p.MapStyle["subHeaderStyle"],
			},
			"K": {
				"Value":     "Result Duplicate",
				"MergeTill": "L",
				"Styles":    p.MapStyle["subHeaderStyle"],
			},
		},
		2: {
			"F": {
				"Value":  "Record",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"G": {
				"Value":  "Duration",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"H": {
				"Value":  "% Duration",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"I": {
				"Value":  "Record",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"J": {
				"Value":  "Duration",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"K": {
				"Value":  "Record",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"L": {
				"Value":  "Duration",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
		},
	}

	excelCustomCmd.SetCellMultiple(f, index, &usedVindex, mapDataL1SummarySubHeader)

	inputTAPIN := 0
	if p.ProcessPropMonthly.MoMt == "mo" {
		inputTAPIN = p.LogReconMonthly.SplitMo
	} else {
		inputTAPIN = p.LogReconMonthly.SplitMt
	}

	inputOCS := 0
	if p.ProcessPropMonthly.MoMt == "mo" {
		inputOCS = p.LogOCSOnly.SplitMo
	} else {
		inputOCS = p.LogOCSOnly.SplitMt
	}

	mapDataL1SummaryBody := map[int]map[string]map[string]interface{}{
		1: {
			"A": {
				"Value":  "DATA TAPIN",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  inputTAPIN,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  p.LogReconMonthly.Duplicate,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  p.LogReconMonthly.LookupReject,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"E": {
				"Value":  (*p.LogCheckDup)["TAPIN"].Unduplicate,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  p.LogReconMonthly.Match,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"G": {
				"Value":  p.LogReconMonthly.MatchDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"H": {
				"Value":  (float32(p.LogReconMonthly.MatchDuration) / float32((*p.LogCheckDup)["TAPIN"].UnduplicateDuration)),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
			"I": {
				"Value":  p.LogReconMonthly.TapinOnly,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"J": {
				"Value":  p.LogReconMonthly.TapinOnlyDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"K": {
				"Value":  (*p.LogCheckDup)["TAPIN"].Duplicate,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"L": {
				"Value":  (*p.LogCheckDup)["TAPIN"].DuplicateDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
		},
		2: {
			"A": {
				"Value":  "DATA OCS",
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  inputOCS,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  p.LogOCSOnly.Duplicate,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  p.LogOCSOnly.LookupReject,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"E": {
				"Value":  (*p.LogCheckDup)["OCS"].Unduplicate,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  p.LogOCSOnly.MatchRecord,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"G": {
				"Value":  p.LogOCSOnly.MatchDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"H": {
				"Value":  (float32(p.LogOCSOnly.MatchDuration) / float32((*p.LogCheckDup)["OCS"].UnduplicateDuration)),
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
			"I": {
				"Value":  p.LogOCSOnly.OnlyRecord,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"J": {
				"Value":  p.LogOCSOnly.OnlyDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"K": {
				"Value":  (*p.LogCheckDup)["OCS"].Duplicate,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"L": {
				"Value":  (*p.LogCheckDup)["OCS"].DuplicateDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
		},
	}

	excelCustomCmd.SetCellMultiple(f, index, &usedVindex, mapDataL1SummaryBody)

	usedVindex++ // skip one row

	mapDataL1MatchHeader := map[string]map[string]interface{}{
		"A": {
			"Value":  "DATA MATCH",
			"Styles": p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDataL1MatchHeader)

	mapDataL1MatchSubHeader := map[int]map[string]map[string]interface{}{
		1: {
			"A": {
				"Value":     "TAPCODE",
				"MergeDown": 1,
				"Styles":    p.MapStyle["subHeaderStyleYellow"],
			},
			"B": {
				"Value":     "Result Match",
				"MergeTill": "D",
				"Styles":    p.MapStyle["subHeaderStyle"],
			},
			"E": {
				"Value":     "Deviasi Duration",
				"MergeTill": "F",
				"Styles":    p.MapStyle["subHeaderStyle"],
			},
		},
		2: {
			"B": {
				"Value":  "Record",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"C": {
				"Value":  "TAPIN Duration",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"D": {
				"Value":  "OCS Duration",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"E": {
				"Value":  "Duration",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"F": {
				"Value":  "%",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
		},
	}

	excelCustomCmd.SetCellMultiple(f, index, &usedVindex, mapDataL1MatchSubHeader)

	for partnerID, log := range *p.LogRecon {
		if p.ProcessPropMonthly.MoMt == "mo" && strings.Contains(p.ProcessPropMonthly.FilterTapCode, partnerID) == false {
			continue
		}
		deviasiDur := math.Abs(float64(log.TapinMatchDuration) - float64(log.OcsMatchDuration))
		var percentDeviasiDur float64
		if deviasiDur == 0 {
			percentDeviasiDur = 0
		} else {
			percentDeviasiDur = math.Abs(float64(log.TapinMatchDuration)-float64(log.OcsMatchDuration)) / (float64(log.TapinMatchDuration) + float64(log.OcsMatchDuration))
		}

		mapDataL1MatchBody := map[string]map[string]interface{}{
			"A": {
				"Value":  partnerID,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  log.OcsMatchRecord,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  log.TapinMatchDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  log.OcsMatchDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"E": {
				"Value":  deviasiDur,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  percentDeviasiDur,
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
		}
		excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDataL1MatchBody)
	}

	usedVindex++ // skip one row

	mapDataL1OneSideHeader := map[string]map[string]interface{}{
		"A": {
			"Value":  "DATA ONESIDE",
			"Styles": p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDataL1OneSideHeader)

	mapDataL1OneSideSubHeader := map[int]map[string]map[string]interface{}{
		1: {
			"A": {
				"Value":     "TAPCODE",
				"MergeDown": 1,
				"Styles":    p.MapStyle["subHeaderStyleYellow"],
			},
			"B": {
				"Value":     "Oneside TAPIN",
				"MergeTill": "D",
				"Styles":    p.MapStyle["subHeaderStyle"],
			},
			"E": {
				"Value":     "Oneside OCS",
				"MergeTill": "G",
				"Styles":    p.MapStyle["subHeaderStyle"],
			},
			"H": {
				"Value":     "Deviasi (TAPIN-OCS)",
				"MergeTill": "J",
				"Styles":    p.MapStyle["subHeaderStyle"],
			},
		},
		2: {
			"B": {
				"Value":  "Record",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"C": {
				"Value":  "Duration",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"D": {
				"Value":  "Chg",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"E": {
				"Value":  "Record",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"F": {
				"Value":  "Duration",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"G": {
				"Value":  "Chg",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"H": {
				"Value":  "Record",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"I": {
				"Value":  "Duration",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"J": {
				"Value":  "Chg",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
		},
	}

	excelCustomCmd.SetCellMultiple(f, index, &usedVindex, mapDataL1OneSideSubHeader)

	for partnerID, log := range *p.LogRecon {
		if p.ProcessPropMonthly.MoMt == "mo" && strings.Contains(p.ProcessPropMonthly.FilterTapCode, partnerID) == false {
			continue
		}
		deviasiDur := math.Abs(float64(log.TapinOnlyDuration) - float64(log.OcsOnlyDuration))
		deviasiRec := math.Abs(float64(log.TapinOnlyRecord) - float64(log.OcsOnlyRecord))
		deviasiRevenue := log.TapinOnlyRevenue - log.OcsOnlyRevenue
		// var percentDeviasiDur float64
		// if deviasiDur == 0 {
		// 	percentDeviasiDur = 0
		// } else {
		// 	percentDeviasiDur = math.Abs(float64(log.TapinMatchDuration)-float64(log.OcsMatchDuration)) / (float64(log.TapinMatchDuration) + float64(log.OcsMatchDuration))
		// }

		mapDataL1OneSideBody := map[string]map[string]interface{}{
			"A": {
				"Value":  partnerID,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  log.TapinOnlyRecord,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  log.TapinOnlyDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  log.TapinOnlyRevenue,
				"Styles": p.MapStyle["bodyStyleFloat"],
			},
			"E": {
				"Value":  log.OcsOnlyRecord,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  log.OcsOnlyDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"G": {
				"Value":  log.OcsOnlyRevenue,
				"Styles": p.MapStyle["bodyStyleFloat"],
			},
			"H": {
				"Value":  deviasiRec,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"I": {
				"Value":  deviasiDur,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"J": {
				"Value":  deviasiRevenue,
				"Styles": p.MapStyle["bodyStyleFloat"],
			},
		}
		excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDataL1OneSideBody)
	}
}

// MakeThirdSheet :
func (p *ReportMaker) MakeThirdSheet(f *excelize.File) {
	usedVindex := 0
	// Create a new sheet.
	index := f.NewSheet("Sheet3")
	f.SetSheetName("Sheet3", "Page 3 - Data L2 per TAPCODE")
	f.SetColWidth("Page 3 - Data L2 per TAPCODE", "A", "A", 15)
	f.SetColWidth("Page 3 - Data L2 per TAPCODE", "B", "B", 15)
	f.SetColWidth("Page 3 - Data L2 per TAPCODE", "C", "D", 15)
	f.SetColWidth("Page 3 - Data L2 per TAPCODE", "E", "E", 15)
	f.SetColWidth("Page 3 - Data L2 per TAPCODE", "F", "G", 15)
	f.SetColWidth("Page 3 - Data L2 per TAPCODE", "H", "H", 15)
	f.SetColWidth("Page 3 - Data L2 per TAPCODE", "I", "S", 15)
	f.SetColWidth("Page 3 - Data L2 per TAPCODE", "T", "T", 15)

	mapReconTitle := map[string]map[string]interface{}{
		"A": {
			"Value":     "RECONCILLIATION REPORTS",
			"MergeTill": "E",
			"Styles":    p.MapStyle["partHeaderNoBorder"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapReconTitle)

	mapHeader2 := map[int]map[string]map[string]interface{}{
		1: {
			"A": {
				"Value":  "Recon Call Type",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     "Roaming Voice  -  " + strings.ToUpper(p.ProcessPropMonthly.MoMt) + " Only",
				"MergeTill": "E",
				"Styles":    p.MapStyle["style"],
			},
		},
		2: {
			"A": {
				"Value":  "Recon Runtime",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     p.TimeParse.Format("January 2006"),
				"MergeTill": "E",
				"Styles":    p.MapStyle["style"],
			},
		},
		4: {
			"A": {
				"Value":  "Recon TAPCODE",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     p.ProcessPropMonthly.FilterTapCode,
				"MergeTill": "E",
				"Styles":    p.MapStyle["style"],
			},
		},
		5: {
			"A": {
				"Value":  "PAGE",
				"Styles": p.MapStyle["style"],
			},
			"B": {
				"Value":     "Page 3 - Data L2 by TAPCODE",
				"MergeTill": "E",
				"Styles":    p.MapStyle["style"],
			},
		},
	}

	excelCustomCmd.SetCellMultiple(f, index, &usedVindex, mapHeader2)

	usedVindex++ // skip one row

	mapDataL2Header := map[string]map[string]interface{}{
		"A": {
			"Value":     "TAPIN :: DATA L2 - BREAKDOWN MATCH",
			"MergeTill": "C",
			"Styles":    p.MapStyle["partHeaderNoBorder"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDataL2Header)

	usedVindex++ // skip one row

	mapDataL1MatchHeader := map[string]map[string]interface{}{
		"A": {
			"Value":  "DATA MATCH",
			"Styles": p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDataL1MatchHeader)

	mapDataL1MatchSubHeader := map[int]map[string]map[string]interface{}{
		1: {
			"A": {
				"Value":     "TAPCODE",
				"MergeDown": 1,
				"Styles":    p.MapStyle["subHeaderStyleYellow"],
			},
			"B": {
				"Value":     "Result Match",
				"MergeTill": "D",
				"Styles":    p.MapStyle["subHeaderStyle"],
			},
			"E": {
				"Value":     "Deviasi Duration",
				"MergeTill": "F",
				"Styles":    p.MapStyle["subHeaderStyle"],
			},
		},
		2: {
			"B": {
				"Value":  "Record",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"C": {
				"Value":  "TAPIN Duration",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"D": {
				"Value":  "OCS Duration",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"E": {
				"Value":  "Duration",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"F": {
				"Value":  "%",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
		},
	}

	excelCustomCmd.SetCellMultiple(f, index, &usedVindex, mapDataL1MatchSubHeader)

	for partnerID, log := range *p.LogRecon {
		if p.ProcessPropMonthly.MoMt == "mo" && strings.Contains(p.ProcessPropMonthly.FilterTapCode, partnerID) == false {
			continue
		}
		deviasiDur := math.Abs(float64(log.TapinMatchDuration) - float64(log.OcsMatchDuration))
		var percentDeviasiDur float64
		if deviasiDur == 0 {
			percentDeviasiDur = 0
		} else {
			percentDeviasiDur = math.Abs(float64(log.TapinMatchDuration)-float64(log.OcsMatchDuration)) / (float64(log.TapinMatchDuration) + float64(log.OcsMatchDuration))
		}

		mapDataL1MatchBody := map[string]map[string]interface{}{
			"A": {
				"Value":  partnerID,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  log.OcsMatchRecord,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  log.TapinMatchDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  log.OcsMatchDuration,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"E": {
				"Value":  deviasiDur,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  percentDeviasiDur,
				"Styles": p.MapStyle["bodyStylePercentage"],
			},
		}
		excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDataL1MatchBody)
	}

	usedVindex++ // skip one row

	mapDataToleransiDurasiHeader := map[string]map[string]interface{}{
		"A": {
			"Value":  "TOLERANSI DURASI",
			"Styles": p.MapStyle["boldOnly"],
		},
	}
	excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDataToleransiDurasiHeader)

	mapDataToleransiDurasiSubHeader := map[int]map[string]map[string]interface{}{
		1: {
			"A": {
				"Value":     "TAPCODE",
				"MergeDown": 1,
				"Styles":    p.MapStyle["subHeaderStyleYellow"],
			},
			"B": {
				"Value":     "Record",
				"MergeTill": "F",
				"Styles":    p.MapStyle["subHeaderStyle"],
			},
			"G": {
				"Value":     "Durasi",
				"MergeTill": "K",
				"Styles":    p.MapStyle["subHeaderStyle"],
			},
		},
		2: {
			"B": {
				"Value":  "0s",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"C": {
				"Value":  "1-30s",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"D": {
				"Value":  "31-60s",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"E": {
				"Value":  "61-120s",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"F": {
				"Value":  "> 120s",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"G": {
				"Value":  "0s",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"H": {
				"Value":  "1-30s",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"I": {
				"Value":  "31-60s",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"J": {
				"Value":  "61-120s",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
			"K": {
				"Value":  "> 120s",
				"Styles": p.MapStyle["subHeaderStyle"],
			},
		},
	}

	excelCustomCmd.SetCellMultiple(f, index, &usedVindex, mapDataToleransiDurasiSubHeader)

	for partnerID, log := range *p.LogMatchToleransiGroup {
		if p.ProcessPropMonthly.MoMt == "mo" && strings.Contains(p.ProcessPropMonthly.FilterTapCode, partnerID) == false {
			continue
		}
		mapDataToleransiDurasiBody := map[string]map[string]interface{}{
			"A": {
				"Value":  partnerID,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"B": {
				"Value":  log.Record0,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"C": {
				"Value":  log.Record30,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"D": {
				"Value":  log.Record60,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"E": {
				"Value":  log.Record120,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"F": {
				"Value":  log.RecordBigger120,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"G": {
				"Value":  log.Duration0,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"H": {
				"Value":  log.Duration30,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"I": {
				"Value":  log.Duration60,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"J": {
				"Value":  log.Duration120,
				"Styles": p.MapStyle["bodyStyle"],
			},
			"K": {
				"Value":  log.DurationBigger120,
				"Styles": p.MapStyle["bodyStyle"],
			},
		}
		excelCustomCmd.SetCellHorizontal(f, index, &usedVindex, mapDataToleransiDurasiBody)
	}
}
