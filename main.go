package main

import (
	"bitbucket.org/billing/go-data-only/app"
	"bitbucket.org/billing/go-data-only/flow"
)

func main() {
	for {
		app.Initialize()

		flow.DataOnly()

		flow.ReportDaily()

		flow.ReportMonthly()

		app.Sleeping()
	}
}
