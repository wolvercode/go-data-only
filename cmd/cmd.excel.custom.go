package cmd

import (
	"sort"
	"strconv"
	"strings"

	"github.com/360EntSecGroup-Skylar/excelize"
)

// ExcelCustomCmd :
type ExcelCustomCmd struct{}

// ExcelCustomStyle :
var ExcelCustomStyle map[string]int

// NewExcel :
func (p *ExcelCustomCmd) NewExcel() (*excelize.File, error) {
	var err error

	f := excelize.NewFile()
	ExcelCustomStyle = make(map[string]int)
	ExcelCustomStyle["style"], err = f.NewStyle(`{"fill":{"type":"pattern","color":["#E0EBF5"],"pattern":1},"font":{"bold":true}}`)
	if err != nil {
		return nil, err
	}
	ExcelCustomStyle["boldOnly"], err = f.NewStyle(`{"font":{"bold":true}}`)
	if err != nil {
		return nil, err
	}
	ExcelCustomStyle["partHeaderNoBorder"], err = f.NewStyle(`{"fill":{"type":"pattern","color":["#B3BCC4"],"pattern":1},"font":{"bold":true}}`)
	if err != nil {
		return nil, err
	}
	ExcelCustomStyle["partHeaderWithBorder"], err = f.NewStyle(`{"fill":{"type":"pattern","color":["#B3BCC4"],"pattern":1},"font":{"bold":true},"border":[{"type":"left","color":"#000000","style":1},{"type":"right","color":"#000000","style":1},{"type":"top","color":"#000000","style":1},{"type":"bottom","color":"#000000","style":1}]}`)
	if err != nil {
		return nil, err
	}
	ExcelCustomStyle["mainHeaderStyleDark"], err = f.NewStyle(`{"fill":{"type":"pattern","color":["#9ec9cf"],"pattern":1},"font":{"bold":true},"alignment":{"horizontal":"center"}}`)
	if err != nil {
		return nil, err
	}
	ExcelCustomStyle["mainHeaderStyleLight"], err = f.NewStyle(`{"fill":{"type":"pattern","color":["#B0E0E6"],"pattern":1},"font":{"bold":true},"alignment":{"horizontal":"center"}}`)
	if err != nil {
		return nil, err
	}
	ExcelCustomStyle["subHeaderStyle"], err = f.NewStyle(`{"fill":{"type":"pattern","color":["#cccccc"],"pattern":1},"font":{"bold":true},"alignment":{"horizontal":"center"},"border":[{"type":"left","color":"#000000","style":1},{"type":"right","color":"#000000","style":1},{"type":"top","color":"#000000","style":1},{"type":"bottom","color":"#000000","style":1}]}`)
	if err != nil {
		return nil, err
	}
	ExcelCustomStyle["subHeaderStyleYellow"], err = f.NewStyle(`{"fill":{"type":"pattern","color":["#ffff00"],"pattern":1},"font":{"bold":true},"alignment":{"horizontal":"center"},"border":[{"type":"left","color":"#000000","style":1},{"type":"right","color":"#000000","style":1},{"type":"top","color":"#000000","style":1},{"type":"bottom","color":"#000000","style":1}]}`)
	if err != nil {
		return nil, err
	}
	ExcelCustomStyle["subHeaderStylePercentage"], err = f.NewStyle(`{"fill":{"type":"pattern","color":["#cccccc"],"pattern":1},"font":{"bold":true},"alignment":{"horizontal":"center"},"border":[{"type":"left","color":"#000000","style":1},{"type":"right","color":"#000000","style":1},{"type":"top","color":"#000000","style":1},{"type":"bottom","color":"#000000","style":1}],"number_format":10}`)
	if err != nil {
		return nil, err
	}
	ExcelCustomStyle["footerStyle"], err = f.NewStyle(`{"fill":{"type":"pattern","color":["#cccccc"],"pattern":1},"font":{"bold":true},"border":[{"type":"left","color":"#000000","style":1},{"type":"right","color":"#000000","style":1},{"type":"top","color":"#000000","style":1},{"type":"bottom","color":"#000000","style":1}],"number_format":3}`)
	if err != nil {
		return nil, err
	}
	ExcelCustomStyle["bodyStyle"], err = f.NewStyle(`{"border":[{"type":"left","color":"#000000","style":1},{"type":"right","color":"#000000","style":1},{"type":"top","color":"#000000","style":1},{"type":"bottom","color":"#000000","style":1}],"number_format":3}`)
	if err != nil {
		return nil, err
	}
	ExcelCustomStyle["bodyStylePercentage"], err = f.NewStyle(`{"border":[{"type":"left","color":"#000000","style":1},{"type":"right","color":"#000000","style":1},{"type":"top","color":"#000000","style":1},{"type":"bottom","color":"#000000","style":1}],"number_format":10}`)
	if err != nil {
		return nil, err
	}
	ExcelCustomStyle["bodyStyleFloat"], err = f.NewStyle(`{"border":[{"type":"left","color":"#000000","style":1},{"type":"right","color":"#000000","style":1},{"type":"top","color":"#000000","style":1},{"type":"bottom","color":"#000000","style":1}],"number_format":2}`)
	if err != nil {
		return nil, err
	}
	ExcelCustomStyle["bodyBoldStyle"], err = f.NewStyle(`{"font":{"bold":true},"border":[{"type":"left","color":"#000000","style":1},{"type":"right","color":"#000000","style":1},{"type":"top","color":"#000000","style":1},{"type":"bottom","color":"#000000","style":1}],"number_format":3}`)
	if err != nil {
		return nil, err
	}

	return f, nil
}

// SetCellHorizontal :
func (p *ExcelCustomCmd) SetCellHorizontal(f *excelize.File, index int, usedVindex *int, mapOfCell map[string]map[string]interface{}) {
	*usedVindex++
	for k, v := range mapOfCell {
		if _, ok := v["Value"]; ok {
			f.SetCellValue(f.GetSheetName(index), k+strconv.Itoa(*usedVindex), v["Value"])
		}
		if _, ok := v["MergeTill"]; ok {
			f.MergeCell(f.GetSheetName(index), k+strconv.Itoa(*usedVindex), v["MergeTill"].(string)+strconv.Itoa(*usedVindex))
		}
		if _, ok := v["Styles"]; ok {
			if _, ok := v["MergeTill"]; ok {
				f.SetCellStyle(f.GetSheetName(index), k+strconv.Itoa(*usedVindex), v["MergeTill"].(string)+strconv.Itoa(*usedVindex), v["Styles"].(int))
			} else {
				f.SetCellStyle(f.GetSheetName(index), k+strconv.Itoa(*usedVindex), k+strconv.Itoa(*usedVindex), v["Styles"].(int))
			}
		}
	}
}

// SetCellMultiple :
func (p *ExcelCustomCmd) SetCellMultiple(f *excelize.File, index int, usedVindex *int, mapOfCell map[int]map[string]map[string]interface{}) {
	// Melakukan Sorting Key agar Cell diisi Berurutan sesuai key dari mapOfCell
	var keys []int
	for k := range mapOfCell {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	// Loop keys yang telah ter-sorting untuk mendapatkan value dari mapOfCell
	for _, k := range keys {
		*usedVindex++
		for keyCell, propCell := range mapOfCell[k] {
			cellKeys := strings.Split(keyCell, ":")
			for _, valuesCellKeys := range cellKeys {
				if _, ok := propCell["Value"]; ok {
					f.SetCellValue(f.GetSheetName(index), valuesCellKeys+strconv.Itoa(*usedVindex), propCell["Value"])
				}
				if _, ok := propCell["MergeTill"]; ok {
					f.MergeCell(f.GetSheetName(index), valuesCellKeys+strconv.Itoa(*usedVindex), propCell["MergeTill"].(string)+strconv.Itoa(*usedVindex))
				}
				if _, ok := propCell["MergeDown"]; ok {
					f.MergeCell(f.GetSheetName(index), valuesCellKeys+strconv.Itoa(*usedVindex), valuesCellKeys+strconv.Itoa((*usedVindex+propCell["MergeDown"].(int))))
				}
				if _, ok := propCell["Styles"]; ok {
					if _, ok := propCell["MergeTill"]; ok {
						f.SetCellStyle(f.GetSheetName(index), valuesCellKeys+strconv.Itoa(*usedVindex), propCell["MergeTill"].(string)+strconv.Itoa(*usedVindex), propCell["Styles"].(int))
					} else if _, ok := propCell["MergeDown"]; ok {
						f.SetCellStyle(f.GetSheetName(index), valuesCellKeys+strconv.Itoa(*usedVindex), valuesCellKeys+strconv.Itoa((*usedVindex+propCell["MergeDown"].(int))), propCell["Styles"].(int))
					} else {
						f.SetCellStyle(f.GetSheetName(index), valuesCellKeys+strconv.Itoa(*usedVindex), valuesCellKeys+strconv.Itoa(*usedVindex), propCell["Styles"].(int))
					}
				}
			}
		}
	}
}
